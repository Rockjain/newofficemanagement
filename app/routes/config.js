
var environments ={}

environments.staging={
    app: {
        port: 3002
      },
  envName:'staging',
      db: {
          dbname:"salarymanagement",
          username:"root",
          password:"",
          host:"localhost",
          dialect:"mysql",
          pool:{
              max:5, 
              mib:0,
              acquire:30000,
              idle: 10000
          }
      }
     
}

environments.production = {
    app: {
        port: 3002
      },
  envName:'production',
      db: {
          dbname:"emp_management",
          username:"fbdev",
          password:"Qwe1@3qwe",
          host:"localhost",
          dialect:"mysql",
          pool:{
              max:5, 
              mib:0,
              acquire:30000,
              idle: 10000
          }
      }
}


// Determine which environment was passed as a command-line argument
var currentEnvironment = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '';

// Check that the current environment is one of the environments above, if not default to staging
var config = typeof(environments[currentEnvironment]) == 'object' ? environments[currentEnvironment] : environments.staging;

console.log(config)

module.exports = config;