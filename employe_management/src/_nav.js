export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name:'Clients',
      url:'/client',
      icon:'fa fa-handshake-o'
    },
    {
      name:'Projects',
      url:'/project',
      icon:'fa fa-file-powerpoint-o'
    },
    {
      name: 'Tasks',
      url: '/taskmanage',
      icon: 'fa fa-tasks',
    },
    {
      name: 'Users',
      url:'/users',
      icon:'fa fa-user',
    },
    {
      name: 'Employees',
      url: '/employee',
      icon: 'icon-people',
    },
    {
      name: 'Salary',
      // url: '/transaction',
      icon:'fa fa-inr',
      children: [
        {
          name: 'Salary',
          url: '/transaction',
          icon:'fa fa-inr',
        },
        {
          name: 'Salary Setup',
          url: '/salary',
          icon:'fa fa-inr'
        },
      ]
    },  
    {
      name: 'Report',
      url: '/report',
      icon:'fa fa-file-text',
      children: [
        {
          name: 'Salary Report',
          url: '/report/salaryreport',
          icon: 'fa fa-file-text',
        },
        {
          name: 'Financial Report',
          url: '/report/finalreport',
          icon: 'fa fa-file-text',
        }]
    },
    {
      name: 'Settings',
      icon: 'fa fa-cog',
          children:[
            {
              name:'Roles & Permission',
              url:'/roles',
            icon:'fa fa-users',
            },
            {         
                name: 'Department',
                url: '/department',
                icon:'icon-plus'
            }
          ]
    },  
  
    {
      divider: true,
    },
 
  ],
};
