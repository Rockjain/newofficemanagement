import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Salary from './salary';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><Salary /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});
