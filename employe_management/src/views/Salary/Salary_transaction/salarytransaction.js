/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button,Form, Input, InputGroup, InputGroupAddon, Table,} from 'reactstrap';
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Moment from 'react-moment';
import './salary.css';
import html2pdf from 'html2pdf.js'
import Pagination from "react-js-pagination";
import config from '../../../port.js';

class SalaryTransition extends Component{
    constructor(props){
        super(props)
        this.state = ({employee:[],month:"", year:"",pdfdata:[],generate:false,delete:true,activePage: 1,pageSize:10,pageNumber:"",currentIndex:1,items:[]})
       
    }

    refreshItems=()=>{
        let items = this.state.employee.slice((this.state.currentIndex - 1)*this.state.pageSize, (this.state.currentIndex) * this.state.pageSize);
       // let pagesIndex =  this.fillArray();
        //console.log(this.items)
        this.setState({items:items})
        console.log("item",this.state.items,this.state.pageSize)
      }


    componentDidMount(){
        let date = new Date();
      
        let year = date.getFullYear();
        let month = date.getMonth()+1;
        this.setState({month:month,year:year,startDate:date})
        console.log(date,month,year)
        axios.get(config.port+"/users/salaryTransaction",{params:{month:month,year:year}}).then(res=>{
            console.log(res.data)
            this.setState({employee:res.data})
            this.refreshItems();
        })
    }
   
    handlePageChange=(pageNumber)=>{
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage:pageNumber,currentIndex:pageNumber});
        console.log("index",this.state.currentIndex)
       this.refreshItems();

       var ref = document.getElementById("salary")
                   
       var getSalary = ref.getElementsByClassName('d-check')
       for(let i=0;i<  getSalary.length;i++){
        getSalary[i].checked = false  
        }

      }

    selectDate=(maskedValue,selectedYear,selectedMonth)=>{

        console.log(maskedValue,selectedYear,selectedMonth)
        this.setState({month:selectedMonth+1, year:selectedYear})
        axios.get(config.port+"/users/salaryTransaction",{params:{month:selectedMonth+1,year:selectedYear}}).then(res=>{
            console.log(res.data)
            this.setState({employee:res.data})
        })
    }

    myChangeHandler = (event) => {
        let val = event.target.value;
        console.log("val",val)
        this.setState({pageSize:val,currentIndex:1});
        this.refreshItems();
        console.log(this.state.pageSize)
    }

    handleChangeStart=(date)=>{
        let month = date.getMonth()+1;
        let year = date.getFullYear();
       
        this.setState({
            startDate: date,month:month,year:year
          });

          axios.get(config.port+"/users/salaryTransaction",{params:{month:month,year:year}}).then(res=>{
            console.log(res.data)
            this.setState({employee:res.data})
        })
    }

    handleCheck=(id)=>{
       
        axios.post(config.port+"/users/togglepaid",{id:id}).then(res=>{
            console.log(res.data)
            if(res.data.success===true){
                axios.get(config.port+"/users/salaryTransaction",{params:{month:this.state.month,year:this.state.year}}).then(res=>{
                    console.log(res.data)
                    this.setState({employee:res.data})
                })
            }
        })
    }

    onchangecheckbox=()=>{
        var ref = document.getElementById("salary")
        let checkdepartment =0
        var getdepartment = ref.getElementsByClassName('d-check')
        for(let i=0;i<getdepartment.length;i++){
            if(getdepartment[i].checked === true){
                checkdepartment++ 
              }   
        }
        if(checkdepartment >0){
            this.setState({generate:true,delete:false}) 
        }else{
            this.setState({generate:false,delete:true})
        }
    }

    multiplepdf=()=>{
        var ref = document.getElementById("salary")
        let ids=[];
         var getdepartment = ref.getElementsByClassName('d-check')
         for(let i=0;i<getdepartment.length;i++){
             
             if(getdepartment[i].checked === true){
                 let id={id:getdepartment[i].id}
                 ids.push(id);
               }
              
         }
         console.log(ids)
         let s_id = JSON.stringify(ids)
         axios.get(config.port+"/users/no_of_salaryslip",{params:{id:s_id}}).then(res=>{
             console.log(res.data)
              this.setState({pdfdata:res.data})
                const input = document.getElementById('pdf');
    
                var opt= {
                    filename:     'salary.pdf',
                    pagebreak: {mode: 'avoid-all', after: '.page-break' }   
                };

                 html2pdf().set(opt).from(input).save();
         })
    }

    generatepdf=(id)=>{
        console.log(id)
       axios.get(config.port+"/users/salaryslip",{params:{id:id}}).then(res=>{
           console.log(res.data)

                this.setState({pdfdata:res.data})
                if(res.data){
                    const input = document.getElementById('pdf');
                    console.log(input)

                    var opt= {
                        filename:     'salary.pdf',
                        pagebreak: {mode: 'avoid-all', after: '.page-break' }   
                    };

                     html2pdf().set(opt).from(input).save();
                    
                    }
                   
            })
        
        
    }

    search=(event)=>{
        let val = event.target.value;
        //console.log(nam,val)
        var queryResult=[];
        if(val !== ""){
        axios.get(config.port+"/users/searchTransaction",{params:{month:this.state.month,year:this.state.year,val:val}}).then(res=>{
      
            res.data.map((rows,index)=>{
            queryResult.push(rows);
            console.log("query result ",queryResult);
        })
        this.setState({employee:queryResult})
        })
        }else{
            this.refreshItems();
        }
        
    }

    render(){
        
         const Capitalize=(str)=>{
            return str.charAt(0).toUpperCase() + str.slice(1);
            }

            
        const paginationContent = (
            <div className="ml-5">
             <Pagination
                hideDisabled
                prevPageText='prev'
                nextPageText='next'
                firstPageText='first'
                lastPageText='last'
                activePage={this.state.activePage}
                itemsCountPerPage={this.state.pageSize}
                totalItemsCount={this.state.employee.length}
                itemClass="page-item"
                linkClass="page-link"
                onChange={this.handlePageChange}
                />
              </div>
          )
    
          var pagination;
          if(this.state.employee.length > this.state.pageSize){
            pagination = <div>{paginationContent}</div>
          }else{
            pagination = <div></div>
          }

      const PDF = (
        <div id="pdf" style={{width:"88%"}} className=" mt-5 mb-5 ml-5 mr-1">
        {this.state.pdfdata.map((data,key)=>{

            return(
                
                <div>
                <h5 className="text-center w-100 p-1">Forebear Production Pvt. Ltd.</h5>
               <h5 className="text-center w-100"> PAY SLIP FOR THE MONTH =><Moment format="MMMM">{Capitalize(data.paid_date)}</Moment></h5>
            <Table bordered key={key} size="sm">
                
                <tr>
                    <td >Name</td>
                    <td colspan="4">{Capitalize(data.name)}</td>
                </tr>
                <tr>
                    <td>Father/Husband's Name</td>
                    <td colspan="4">{Capitalize(data.father_name)}</td>
                    </tr>
                <tr>
                    <td>Address</td>
                    <td colspan="4">{data.local_address_l1} {data.local_address_l2} {data.local_city} {data.local_state} {data.local_country} {data.pincode}</td>
                    </tr>
                <tr>
                    <td>Department</td>
                    <td colspan="4">{data.department}</td>
                </tr>
                <tr>
                    <td>Designation</td>
                    <td colspan="4">{Capitalize(data.designation)}</td>
                </tr>

                <tr>
                    <th>Earning</th>
                    <th>Amount</th>
                    <th></th>
                    <th>Deduction</th>
                    <th>Amount</th>
                </tr>
                <tr>
                    <td>Basic Salary & DA</td>
                    <td className="text-right">{data.basic_salary}</td>
                    <td></td>
                    <td>Employer PF Contribution</td>
                    <td className="text-right">{data.empr_pf_contri} </td>
                    </tr>
                <tr>
                    <td>HRA</td>
                    <td className="text-right">{data.hra}</td>
                    <td></td>
                    <td>Employee PF Contribution</td>
                    <td className="text-right">{data.emp_pf_contri}</td>
                    </tr>
                <tr>
                    <td>Transportation Allowance</td>
                    <td className="text-right">{data.transportation_allowance}</td>
                    <td></td>
                    <td>TDS</td>
                    <td className="text-right">{data.tds}</td>
                
                    </tr>
                <tr>
                    <td>Medical Allowance</td>
                    <td className="text-right">{data.medical_allowance }</td>
                    <td></td>
                    <td>Absent(No. of Days)</td>
                    <td className="text-right">{data.absent_short_time} </td>
                    
                    </tr>
                <tr>
                    <td>Bonus(Monthly basis)</td>
                    <td className="text-right">{data.bonus} </td>
                    <td></td>
                    <td>Advance</td>
                    <td className="text-right">{data.advance}</td>
                    </tr>
                <tr>
                    <th>TOTAL EARNINGS Rs.</th>
                    <th className="text-right">
                    {
                        parseInt(data.basic_salary)+ parseInt(data.hra)+ parseInt(data.medical_allowance)+ parseInt(data.transportation_allowance)+ parseInt(data.bonus)
                    }
                    </th>
                    <th></th>
                    <th className="text-right">TOTAL DEDUCTION Rs.</th>
                    <th className="text-right">
                        { parseInt(data.empr_pf_contri)+ parseInt(data.emp_pf_contri)+ parseInt(data.esi_of_empr_contri)+ parseInt(data.esi_of_emp_contri)+parseInt(data.tds)+parseInt(data.advance)+parseInt(data.absent_short_time)}
                    </th>
                    </tr>
                <tr>
                    <td>Arrear/ Incentives</td>
                    <td className="text-right">0</td>
                    <th></th>
                    <th></th>
                    <th></th>
                    </tr>
                <tr>
                    <th>Net Pay Rs.=></th>
                    <th>{data.total_paid}</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
             
            </Table>
            {key === 2 ?<div className="page-break"></div>:null}
            </div>
             )
            })}
            </div>
      )

      const salaryDiv = (
        <Form id="salary">
        <div>
       <Row>
         
       <InputGroup className="mb-2">
         <InputGroupAddon addonType="prepend"><Button><i className="fa fa-search"></i></Button></InputGroupAddon>
         <Input placeholder="Search" name="searchbar" className="w-50" onChange={this.search}/>
          <Input type="select"  name="pageSize" id="pageSize" onChange={this.myChangeHandler} onMouseOut={this.myChangeHandler} >
                                           <option value="10">10</option>
                                           {this.state.employee.length>10?<option value="15">15</option>:<span> </span>}
                                           {this.state.employee.length>15?<option value="20">20</option>:<span> </span>}
                                           {this.state.employee.length>20?<option value="25">25</option>:<span> </span>}
                                           {this.state.employee.length>25?<option value="30">30</option>:<span> </span>}
                                           </Input>
         <DatePicker
           selected={this.state.startDate}
           selectsStart
           startDate={this.state.startDate}
           endDate={this.state.endDate}
           dateFormat="MMMM, yyyy"
           showMonthYearPicker
           onChange={this.handleChangeStart} className="pt-1 pl-1 pb-2 border border-light"/> 
         </InputGroup>
         
        </Row>
     </div>
   <Table>
       <thead>
           <th>S.No</th>
           <th>Employee</th>
           <th>Gender</th>
           <th>Basic Salary</th>
           <th>Salary Paid</th>
           <th>Paid Date</th>
           <th>Status</th>
           <th>Action</th>
       </thead>
       <tbody>
           {this.state.items.map((emp,key)=>{
                var checked;
                emp.paid_status ==="paid"? checked=true:checked=false;
               return(
                   <tr key={key}>
                       <td>
                        <input type="checkbox" className="mr-2 d-check" value={emp.id} id={emp.id}  onChange={this.onchangecheckbox}/>
                        {key+1}</td>
                       <td>{Capitalize(emp.emp_name)} </td>
                       <td>{Capitalize(emp.gender)} </td>
                       <td>{emp.basic_salary}</td>
                       <td>{emp.total_paid}</td>
                       <td><Moment format="DD-MM-YYYY">{emp.paid_date}</Moment> </td>
                       <td>
                          {emp.paid_status === "paid"?<Button type="button" color="primary" size="sm" disabled>{Capitalize(emp.paid_status)}</Button>:<Button type="button" color="danger" size="sm" onClick={()=>this.handleCheck(emp.id)} hidden={checked}>Mark As Paid</Button>} 
                       </td>
                       <td>
                           {emp.paid_status === "paid"?<Button color="success" size="sm" type="button" onClick={()=>this.generatepdf(emp.id)}>Salary Slip</Button>:""}</td>
                   </tr>
               )
           })}
       </tbody>
   </Table>
   <div className="ml-5">
                             {pagination}
                           </div>
   </Form>
      )

      const nosalary = (
        <div>
        <Row>
          
        <InputGroup className="mb-2">
          <InputGroupAddon addonType="prepend"><Button><i className="fa fa-search"></i></Button></InputGroupAddon>
          <Input placeholder="Search" name="searchbar" className="w-50" onChange={this.search}/>
           <Input type="select"  name="pageSize" id="pageSize" onChange={this.myChangeHandler} onMouseOut={this.myChangeHandler} >
                                            <option value="10">10</option>
                                            {this.state.employee.length>10?<option value="15">15</option>:<span> </span>}
                                            {this.state.employee.length>15?<option value="20">20</option>:<span> </span>}
                                            {this.state.employee.length>20?<option value="25">25</option>:<span> </span>}
                                            {this.state.employee.length>25?<option value="30">30</option>:<span> </span>}
                                            </Input>
          <DatePicker
            selected={this.state.startDate}
            selectsStart
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            dateFormat="MMMM, yyyy"
            showMonthYearPicker
            onChange={this.handleChangeStart} className="pt-1 pl-1 pb-2 border border-light"/> 
          </InputGroup>
          
         </Row>
         <h5 className="text-center mt-4 text-primary">No salary transit by you In this month</h5>
      </div>
      );

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4><i className="fa fa-inr"></i>&nbsp; Salary
                            <Link to='/transaction/generate'><Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} hidden={this.state.generate}>Generate</Button>
                            </Link>
                            <a className="btn btn-danger btn-sm ml-2 float-right text-light" size="sm" hidden={this.state.delete} onClick={this.multiplepdf}>Generate PDF</a>
                            </h4>
                        </CardHeader>
                        <CardBody>
                            {this.state.employee.length > 0 ?salaryDiv: nosalary }
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

                                    <div style={{display:"none"}}>
                                        {PDF}
                                    </div>
            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default SalaryTransition;