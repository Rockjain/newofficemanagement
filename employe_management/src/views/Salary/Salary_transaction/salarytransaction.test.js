import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import SalaryTransiton from './salarytransaction';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><SalaryTransiton /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});
