import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import GenerateSalary from './generate';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><GenerateSalary /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});
