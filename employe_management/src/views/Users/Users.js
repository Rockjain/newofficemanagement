import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table, Button } from 'reactstrap';
import axios from 'axios'
import Moment from 'react-moment';
import config from '../../port.js';
import { confirmAlert } from 'react-confirm-alert'; 
import 'react-confirm-alert/src/react-confirm-alert.css';
//import usersData from './UsersData'

// function UserRow(props) { 
//   const user = props.user
//   const userLink = `/users/${user.id}` 

//   const getBadge = (status) => {
//     return status === 'Active' ? 'success' :
//       status === 'Inactive' ? 'secondary' :
//         status === 'Pending' ? 'warning' :
//           status === 'Banned' ? 'danger' :
//             'primary'
//   }

//  const Capitalize=(str)=>{
//     return str.charAt(0).toUpperCase() + str.slice(1);
//     }
    
//   return (
//     <tr key={user.id.toString()}>
//       <th scope="row"><Link to={userLink}>{user.id}</Link></th>
//       <td><Link to={userLink}>{Capitalize(user.username)}</Link></td>
//       <td><Moment format="YYYY/MM/DD">{user.ragistered}</Moment></td>
//       <td>{Capitalize(user.role)}</td>
//       <td><Link to={userLink}><Badge color={getBadge(Capitalize(user.status))}>{Capitalize(user.status)}</Badge></Link></td>
//       <td></td>
//     </tr>
//   )
// }

class Users extends Component {
  constructor(props){
    super(props);
    this.state = ({admin:[]});
  }

  refreshItems=()=>{
    let items = this.state.admin.slice((this.state.currentIndex - 1)*this.state.pageSize, (this.state.currentIndex) * this.state.pageSize);
    this.setState({items:items})
    console.log("item",this.state.items,this.state.pageSize)
  }

  componentDidMount () {
      axios.get(config.port+"/users/user").then(res=>{
        console.log("data",res.data)
        this.setState({admin:res.data})
        console.log(this.state.admin)
      })
  }
  deleteUser=(id)=>{
    axios.delete(config.port+"/users/deleteuser",{params:{id:id}}).then(res=>{
        console.log(res)
        if(res.data.success===true){
          axios.get(config.port+"/users/user").then(res=>{
              console.log(res.data)
              this.setState({admin:res.data})
              this.refreshItems();

               })
        }
    })
  }
  delete=(id,username)=>{
    confirmAlert({
      title: 'User Id:' + id,
      message: 'You Really Want To Delete Employee: ' + username,
      buttons: [
        {
          label: 'Yes',
          onClick: () => this.deleteUser(id)
        },
        {
          label: 'No',          
        }
      ]
    });
  }

 
  render() {

    const getBadge = (status) => {
      return status === 'Active' ? 'success' :
        status === 'Inactive' ? 'secondary' :
          status === 'Pending' ? 'warning' :
            status === 'Banned' ? 'danger' :
              'primary'
      }
  
   const Capitalize=(str)=>{
      return str.charAt(0).toUpperCase() + str.slice(1);
      }

   const userList = this.state.admin

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={6}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Users <small className="text-muted"></small>
                
                <Link to='/users/add'><Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} hidden={this.state.add} >
                <i className="fa fa-plus" style={{color:"black"}}></i></Button></Link>
              </CardHeader>
              <CardBody>
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th scope="col">id</th>
                      <th scope="col">name</th>
                      <th scope="col">registered</th>
                      <th scope="col">role</th>
                      <th scope="col">status</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  {userList.map((user, index) =>{
                    const userLink = `/users/${user.id}`
                    const editLink = `/users/edit/${user.id}`

                    return(
                      <tr key={user.id.toString()}>

                      <th scope="row"><Link to={userLink}>{user.id}</Link></th>
                            <td><Link to={userLink}>{Capitalize(user.username)}</Link></td>
                            <td><Moment format="YYYY-MMM-DD">{user.ragistered}</Moment></td>
                            <td>{Capitalize(user.role)}</td>
                            <td><Link to={userLink}><Badge color={getBadge(Capitalize(user.status))}>{Capitalize(user.status)}</Badge></Link></td>
                            <td>
                            <Link to={editLink}><Button className="btn-sm" color="primary"><i className="fa fa-pencil" style={{color:"white"}}></i></Button></Link>

                            <a className="btn btn-danger btn-sm ml-2" onClick={()=>this.delete(user.id,user.username)}><i className="fa fa-trash" style={{color:"white"}}></i></a>
                            </td>

                       </tr>
                     )

                    }
                      // <UserRow key={index} user={user}/>
                    )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Users;
