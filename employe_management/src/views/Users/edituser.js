import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Table, Form, Button,Input,InputGroup, InputGroupAddon, InputGroupText} from 'reactstrap';
import Moment from  'react-moment';
import { Redirect } from 'react-router-dom'
import config from '../../port.js';


class EditUser extends  Component{ 
    constructor(props){
        super(props);
        this.state = ({employee:[],admin:[],roles:[],emp_id:"",emp_name:"",username:"",ragistered:Date(),password:"",role:"",status:"",redirect:false});
      }


      componentDidMount(){
        axios.get(config.port+"/users/currentemp").then(res=>{
            console.log(res.data)
            this.setState({employee:res.data})
  
          })

        axios.get(config.port+"/users/showroles").then(res=>{
            console.log("data",res.data)
            this.setState({roles:res.data})
            console.log(this.state.roles)
          }) 

        let id = this.props.match.params.id;
        console.log(id)
        axios.get(config.port+"/users/showruserbyid",{params:{id:id}}).then(res=>{
            console.log("data",res.data)
            if(res.data.length>0){
            let admin = res.data[0]
            this.setState({emp_name:admin.emp_name,username:admin.username,ragistered:admin.ragistered,role:admin.role,status:admin.status,password:admin.password})
            }
            console.log(this.state.admin)
          }) 
     
      }

      myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
        console.log(this.state)
      }

      renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/users' />
        }
    }

    handleChange=(date)=>{
        this.setState({
            startDate: date
        });
    }

    UpdateUser=(e) =>{
        e.preventDefault();
        console.log(this.state)
        axios.post(config.port+"/users/updateuser",{
            username:this.state.username,
            password:this.state.password,
            ragistered:this.state.ragistered,
            role:this.state.role,
            status:this.state.status,
            id:this.props.match.params.id
        }).then(res=>{
            if(res.data.success === true){
                this.setState({redirect:true})
            }    
        })

    }

    render(){
        if(localStorage.getItem("token")){
            return (
                <div className="animated fadeIn">
                    
                    <Row>
                        <Col>
                            <Card>
                            <CardHeader>
                                <h4>Edit User</h4>
                            </CardHeader>
                            <CardBody>
                            <Form onSubmit={this.UpdateUser} >
                                <Table borderless>
                                <tbody>   
                                    <tr>
                                        <th>Employee Name :</th>
                                        <td>
                                        <input type="text" name="emp_id" className="w-50"  value={this.state.emp_name}/>                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Username:</th>
                                         <td>
                                           <input type="text" name="username" className="w-50" onChange={this.myChangeHandler} value={this.state.username} placeholder="User Name" autocomplete="username"/>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Password:</th>
                                         <td>
                                         <input type="password" name="password" className="w-50" onChange={this.myChangeHandler} value={this.state.password} placeholder="password" />
  
                                            
                                        </td>
                                     </tr>
                                  <tr>
                                    <th>Registered Date:</th>
                                      <td>  
                                          <input type="date" name="ragistered" onChange={this.myChangeHandler}/>     
                                      </td>
                                  </tr>
                                  <tr>
                                      <th>Role :</th>
                                      <td>
                                            <select name="role" id="role" className="w-50" onChange={this.myChangeHandler} value={this.state.role}>
                                          <option value="0">Please Select User Role Name</option>   
                                          {
                                            this.state.roles.map((roles,key)=>{
                                            return <option value={roles.id}>{roles.role_name}</option>
                                            })
                                            }         
                                          </select>
                                          </td>
                                  </tr>
                                  <tr>
                                      <th>Status :</th>
                                      
                                      <td><select  name="status" id="status" className="w-50" onChange={this.myChangeHandler} value={this.state.status}>
                                                <option value="0">Please Select Status</option>
                                                <option value="active">Active </option>
                                                <option value="inactive">In active</option>
                                                {/* <option value="completed">Completed</option> */}
                                                </select>
                                            </td>
                                       
                                  </tr>
                                             
         
                                         </tbody>
                                </Table>
                                      {this.renderRedirect()}
                                        <Button type="submit" color="primary" className="float-right">Update</Button>
                                         </Form>
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
    
                </div>
            )


        }else{
            return(
                <div>
                   <Row>
                        <Col>
                            <Card>
                                <CardHeader>
                                 <h4>Message</h4>
                                  </CardHeader>
                                  <CardBody>
                                    Sorry you are not loggedin.    
                                </CardBody>
                             </Card>
                        </Col>
                        </Row>
                  </div>
              )
        }
    }


}

export default EditUser;