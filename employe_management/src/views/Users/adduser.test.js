import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import AddUser from './adduser';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><AddUser /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});