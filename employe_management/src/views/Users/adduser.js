import React, { Component } from 'react';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import {  Card, CardBody, CardHeader, Col, Row, Button, Table, Form} from 'reactstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import config from '../../port.js';

class AddUser extends Component{

    constructor(props){
        super(props); 
        this.state = ({employee:[],admin:[],roles:[], id:"",username:"",password:"", startDate:new Date(), role:"", status:"",redirect:false});
      }

      componentDidMount(){       

        axios.get(config.port+"/users/currentemp").then(res=>{
            console.log(res.data)
            this.setState({employee:res.data})
  
          })

        axios.get(config.port+"/users/user").then(res=>{
            console.log("data",res.data)
            this.setState({admin:res.data})
            console.log(this.state.admin)
          })      

          axios.get(config.port+"/users/showroles").then(res=>{
            console.log("data",res.data)
            this.setState({roles:res.data})
            console.log(this.state.roles)
          }) 
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
       console.log(this.state)

    }

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/users' />
        }
    }

    handleChange=(date)=>{
        this.setState({
            startDate: date
        });
    }

    AddUser=(event)=>{
        event.preventDefault();
        console.log(this.state)
        if(this.state.username === "" || this.state.password === "" || this.state.role === "" || this.state.status === ""){
            alert("All fields are required")
          }else{  
            axios.post(config.port+"/users/adduser",{
                emp_id:this.state.emp_id,
                username:this.state.username,
                password:this.state.password,
                ragistered:this.state.startDate,
                role:this.state.role,
                status:this.state.status,
            }).then(res=>{
                if(res.data.success===true){
                    this.setState({redirect:true})
                }
            })  
        }              
    }

    render(){
        if(localStorage.getItem("token")){
            return (
                <div className="animated fadeIn">
                    
                    <Row>
                        <Col>
                            <Card>
                            <CardHeader>
                                <h4>User</h4>
                            </CardHeader>
                            <CardBody>
                            <Form onSubmit={this.AddUser} autoComplete="off">
                                <Table borderless>
                                <tbody>   
                                    <tr>
                                        <th>Employee Name :</th>
                                        <td>
                                        <select name="emp_id" id="emp_id" onChange={this.myChangeHandler} className="w-50">
                                            <option value="0">Please Select Employee Name</option>
                                             {
                                                this.state.employee.map((employee,key)=>{
                                                return <option value={employee.id}>{employee.name}</option>
                                                })
                                             }
                                        </select>
                                        </td>

                                    </tr>
                                    <tr>
                                        <th>Username:</th>
                                         <td>
    
                                           <input type="text" name="username" className="w-50" onChange={this.myChangeHandler} placeholder="User name" />
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Password:</th>
                                         <td>
                                         <input type="password" name="password" className="w-50" onChange={this.myChangeHandler} placeholder="password" />
  
                                            
                                        </td>
                                     </tr>
                                  <tr>
                                    <th>Registered Date:</th>
                                      <td>  
                                          <input type="date" name="ragistered" onChange={this.myChangeHandler}/>     
                                      </td>
                                  </tr>
                                  <tr>
                                      <th>Role :</th>
                                      <td>
                                            <select name="role" id="role" className="w-50" onChange={this.myChangeHandler}>
                                          <option value="0">Please Select User Role Name</option>   
                                          {
                                            this.state.roles.map((roles,key)=>{
                                            return <option value={roles.id}>{roles.role_name}</option>
                                            })
                                            }         
                                          </select>
                                          </td>
                                  </tr>
                                  <tr>
                                      <th>Status :</th>
                                      
                                      <td><select  name="status" id="status" className="w-50" onChange={this.myChangeHandler} required>
                                                <option value="0">Please Select Status</option>
                                                <option value="active">Active </option>
                                                <option value="inactive">In active</option>
                                                {/* <option value="completed">Completed</option> */}
                                                </select>
                                            </td>
                                       
                                  </tr>
                                             
         
                                         </tbody>
                                </Table>
                                      {this.renderRedirect()}
                                         <Button color="primary" className="float-right">Submit</Button>
                                         </Form>
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
    
                </div>
            )


        }else{
            return(
                <div>
                   <Row>
                        <Col>
                            <Card>
                                <CardHeader>
                                 <h4>Message</h4>
                                  </CardHeader>
                                  <CardBody>
                                    Sorry you are not loggedin.    
                                </CardBody>
                             </Card>
                        </Col>
                        </Row>
                  </div>
              )
        }
        
    }

}

export default AddUser;