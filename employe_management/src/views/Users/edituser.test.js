import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import { mount } from 'enzyme'
import EditUser from './edituser';


it('renders without crashing', () => {
  const wrapper = mount(
    <MemoryRouter> 
      <EditUser match={{params: {id: "1"}, isExact: true, path: "/users/edit/:id", name: "Edit user"}}/>
    </MemoryRouter>
  );
  expect(wrapper.containsMatchingElement(<strong>Roles</strong>)).toEqual(true)
  wrapper.unmount()
});