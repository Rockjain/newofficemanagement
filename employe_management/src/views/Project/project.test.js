import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Project from './project';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><Project /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});