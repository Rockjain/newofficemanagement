/* eslint-disable array-callback-return */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Table} from 'reactstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Redirect } from 'react-router-dom';
import config from '../../port.js';

class AddTask extends Component{
    constructor(props){
        super(props)
        this.state = ({employee:[],startDate:new Date(),project:[],task:[],time:[],status:[],emp_id:[],emp_code:[],submit:false,update:true,messgae:"",redirect:false})
        
    }

    componentDidMount(){ 
       
        axios.get(config.port+"/users/currentemp").then(res=>{
            console.log(res.data)
            this.setState({employee:res.data})
            let project=[];
            let task=[];
            let time=[];
            let status=[];
            let emp_id=[];
            let emp_code=[];
            res.data.map((data,index)=>{
                emp_id.push(data.id)
                emp_code.push(data.emp_code)
                project.push("")
                task.push("")
                time.push("")
                status.push("")
            })

            console.log(project,task,time,status,emp_id,emp_code)
            this.setState({project:project,task:task,time:time,status:status,emp_id:emp_id,emp_code:emp_code})
            console.log(this.state.emp_code)
        })
    }

    handleChange=(date)=>{
        this.setState({
          startDate: date
        });
      }
     
      handleSelect=(date)=>{
        this.setState({
          startDate: date
        });
      }

      handleprojectChange=idx=>evt=>{
        const project = this.state.project.map((project, sidx) => {
            if (idx === sidx){
                return evt.target.value ;
                }else{
                    return project  
                }            
          });
         
          this.setState({project:project});
          console.log(this.state.project)
      }

      handletaskChange=idx=>evt=>{
        const task = this.state.task.map((task, sidx) => {
            if (idx === sidx){
                return evt.target.value ;
                }else{
                    return task
                }            
          });
         
          this.setState({task:task});
          console.log(this.state.task)
      }

      handletimeChange=idx=>evt=>{
        const time = this.state.time.map((time, sidx) => {
            if (idx === sidx){
                return evt.target.value ;
                }else{
                    return time 
                }            
          });
         
          this.setState({time:time});
          console.log(this.state.time)
        }

      handelstatuschange=idx=>evt=>{
        const status = this.state.status.map((status, sidx) => {
            if (idx === sidx){
                return evt.target.value ;
                }else{
                    return status  
                }            
          });
         
          this.setState({status:status});
          console.log(this.state.status)
      }

      submitTask=(event)=>{
        
        event.preventDefault();
        for(let i=0; i<this.state.employee.length; i++){
            console.log(this.state.emp_id[i])
            if(this.state.status[i] === ""){
                this.setState({message:"Please Select the Status"})
            }else{
                axios.post(config.port+"/users/addtask",{
                    emp_id:this.state.emp_id[i],
                    emp_code:this.state.emp_code[i],
                    date:this.state.startDate,
                    time:this.state.time[i],
                    project:this.state.project[i],
                    task:this.state.task[i],
                    status:this.state.status[i]
                }).then(res=>{
                    console.log(res.data);
                   if(res.data.success===true){
                       this.setState({redirect:true})
                   }else{
                       this.setState({update:false,submit:true,
                    message:"Data already exist of this date if you want to update then click on update button."})
                   }
                })
        }
    }
      }

      
      renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/task' />
        }
      }

    render(){
         const Capitalize=(str)=>{
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Assigne Task
                                  <span className="float-right"><DatePicker
                                    selected={this.state.startDate}
                                    onChange={this.handleChange}
                                    onSelect={this.handleSelect}
                                    dateFormat="MMMM dd, yyyy"
                                    className=" pl-1 float-right"/> </span>
                            </h4>
                        </CardHeader>
                        <CardBody>
                            <Table>
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Employee name</th>
                                        <th>Project</th>
                                        <th>Task</th>
                                        <th>Time</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                               {this.state.employee.map((emp,key)=>{
                                   let project = `project-${key}`
                                   let time= `time-${key}`
                                   let task = `task-${key}`
                                   let status = `status-${key}`
                                   return(
                                    <tr>
                                        <td>{key+1}</td>
                                        <td>{Capitalize(emp.name)}</td>
                                        <td><input type="text" name={project} value={this.state.project[key]} onChange={this.handleprojectChange(key)} /></td>
                                        <td>
                                            <input type="text" name={task} value={this.state.task[key]} onChange={this.handletaskChange(key)}/>
                                        </td>
                                        <td>
                                        <input type="text" name={time} value={this.state.time[key]} onChange={this.handletimeChange(key)} />
                                        </td>
                                        <td><input type="radio" name={status} onChange={this.handelstatuschange(key)} value="working" onClick={this.handelstatuschange(key)} required/> Working
                                                &nbsp;
                                                <input type="radio" name={status} onChange={this.handelstatuschange(key)}  value="completed" required /> Completed 
                                                 &nbsp;
                                                </td>
                                    </tr>
                                  
                                   )
                               })}
                                  <tr><td colspan="5">
                                     <h6 className="text-warning">{this.state.message}</h6>
                                     </td>
                                     {this.renderRedirect()}
                                     <td>
                                     <Button color="primary" type="button" className="float-right" onClick={this.submitTask}
                                     hidden={this.state.submit}>Submit</Button>
                                     <Button color="primary" type="button" className="float-right" onClick={this.updateTask} hidden={this.state.update}>Update</Button>
                                     </td></tr>
                            </Table>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default AddTask;