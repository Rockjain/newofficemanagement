/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';

import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import config from '../../../port.js';

class Login extends Component {
  constructor(props){
    super(props);
    this.state = ({data:[],username:"",password:"",redirect:false, message:""})
  }

  
  componentDidMount(){
     
  }

  login=(event)=>{
   // alert("you are submitting "+this.state.username+" "+this.state.password)
   event.preventDefault();
    axios.post(config.port+"/users/login",{username:this.state.username,password:this.state.password}).then(res=>{
      console.log(res.data);
      if(res.data.success === true){
      var myHour = new Date();
      myHour.setHours(myHour.getHours() + 12); //one hour from now
      let data=[];
      data.push(myHour);
      localStorage.setItem('storedData', JSON.stringify(data))
      localStorage.setItem("token",res.data.token)
      
      this.setState({redirect:true})
      }
      else{
       this.setState({message:"Incorrect Username and Password."})
      }
    })

  }
  
  myChangeHandler = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  renderRedirect=()=>{
    if (this.state.redirect) {
      return <Redirect to='/dashboard' />
    }
  }

  render() {
    if(localStorage.getItem("token")){
      return <Redirect to="/dashboard" />
    }else{
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Username" name="username" onChange={this.myChangeHandler} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" name="password" onChange={this.myChangeHandler} />
                      </InputGroup>
                        <Row><p className="text-danger ml-3">{this.state.message === null ?<span>&nbsp;</span>:<span>{this.state.message}</span>}</p></Row>
                      <Row>
                     
                        <Col xs="6">
                        {this.renderRedirect()}
                          <Button color="primary" className="px-4" type="button" onClick={this.login}>Login</Button>
                         
                        </Col>
                        <Col xs="6" className="text-right">
                          <a color="link" className="px-0">Forgot password?</a>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
  }
}

export default Login;
