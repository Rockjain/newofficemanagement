import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Form, Table} from 'reactstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Moment from 'react-moment';
import html2pdf from 'html2pdf.js';
import config from '../../port.js';

class SalaryReport extends Component{
    constructor(props){
        super(props)
        this.state = ({employees:[],startDateFrom:"",startDateTo:"",emp_id:"",pdfdata:[]})
       
    }

    componentDidMount(){ 
        let date = new Date();
      
        let year = date.getFullYear();
        let month = date.getMonth()+1;
        this.setState({month:month,year:year,startDateFrom:date,startDateTo:date})
        console.log(date,month,year)

        axios.get(config.port+"/users/showemployee").then(res=>{
            console.log(res.data)
            this.setState({employees:res.data})
        })
    }

      myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
      }

      
    handleChangeStartFrom=(date)=>{
        let month = date.getMonth()+1;
        let year = date.getFullYear();
       
        this.setState({
            startDateFrom:date,month:month,year:year
          });
    }

       
    handleChangeStartTo=(date)=>{
        let month = date.getMonth()+1;
        let year = date.getFullYear();
       
        this.setState({
            startDateTo: date,month:month,year:year
          });
    }

    generateReport=()=>{
        let monthfrom = this.state.startDateFrom.getMonth();
        let yearfrom = this.state.startDateFrom.getFullYear();
        let monthto = this.state.startDateTo.getMonth();
        let yearto = this.state.startDateTo.getFullYear();
        console.log(this.state.emp_id)
        if(this.state.emp_id === "all"){
            axios.get(config.port+"/users/all_salaryslip_by_month",{params:{from:monthfrom,to:monthto,yearfrom:yearfrom,yearto:yearto}}).then(res=>{
                console.log(res.data)
                this.setState({pdfdata:res.data})
                if(res.data){
                    const input = document.getElementById('pdf');
                    console.log(input)
                
                    var opt= {
                            filename:     'salary.pdf',
                            pagebreak: {mode: 'avoid-all', after: '.page-break' }   
                    };

                     html2pdf().set(opt).from(input).save()
                    }
            })
        }else{
            axios.get(config.port+"/users/salaryslip_by_month",{params:{from:monthfrom,to:monthto,yearfrom:yearfrom,yearto:yearto,id:this.state.emp_id}}).then(res=>{
                console.log(res.data)
                this.setState({pdfdata:res.data})
                if(res.data){
                    const input = document.getElementById('pdf');
                    console.log(input)
                    var opt= {
                        filename:     'salary.pdf',
                        pagebreak: {mode: 'avoid-all', after: '.page-break' }   
                    };

                     html2pdf().set(opt).from(input).save();
                    }
            })
        }
    }

    render(){
       
        const Capitalize=(str)=>{
            return str.charAt(0).toUpperCase() + str.slice(1);
            }

      const PDF = (
        <div id="pdf" style={{width:"88%"}} className=" mt-5 mb-5 ml-5 mr-1">
        {this.state.pdfdata.map((data,key)=>{

            return(
                
                <div>
                <h5 className="text-center w-100 p-1">Forebear Production Pvt. Ltd.</h5>
               <h5 className="text-center w-100"> PAY SLIP FOR THE MONTH =><Moment format="MMMM">{Capitalize(data.paid_date)}</Moment></h5>
            <Table bordered key={key} size="sm">
                
                <tr>
                    <td >Name</td>
                    <td colspan="4">{Capitalize(data.name)}</td>
                </tr>
                <tr>
                    <td>Father/Husband's Name</td>
                    <td colspan="4">{Capitalize(data.father_name)}</td>
                    </tr>
                <tr>
                    <td>Address</td>
                    <td colspan="4">{data.local_address_l1} {data.local_address_l2} {data.local_city} {data.local_state} {data.local_country} {data.pincode}</td>
                    </tr>
                <tr>
                    <td>Department</td>
                    <td colspan="4">{data.department}</td>
                </tr>
                <tr>
                    <td>Designation</td>
                    <td colspan="4">{Capitalize(data.designation)}</td>
                </tr>
                <tr>
                    <th>Earning</th>
                    <th>Amount</th>
                    <th></th>
                    <th>Deduction</th>
                    <th>Amount</th>
                </tr>
                <tr>
                    <td>Basic Salary & DA</td>
                    <td className="text-right">{data.basic_salary}</td>
                    <td></td>
                    <td>Employer PF Contribution</td>
                    <td className="text-right">{data.empr_pf_contri} </td>
                    </tr>
                <tr>
                    <td>HRA</td>
                    <td className="text-right">{data.hra}</td>
                    <td></td>
                    <td>Employee PF Contribution</td>
                    <td className="text-right">{data.emp_pf_contri}</td>
                    </tr>
                <tr>
                    <td>Transportation Allowance</td>
                    <td className="text-right">{data.transportation_allowance}</td>
                    <td></td>
                    <td>TDS</td>
                    <td className="text-right">{data.tds}</td>
                
                    </tr>
                <tr>
                    <td>Medical Allowance</td>
                    <td className="text-right">{data.medical_allowance }</td>
                    <td></td>
                    <td>Absent(No. of Days)</td>
                    <td className="text-right">{data.absent_short_time} </td>
                    
                    </tr>
                <tr>
                    <td>Bonus(Monthly basis)</td>
                    <td className="text-right">{data.bonus} </td>
                    <td></td>
                    <td>Advance</td>
                    <td className="text-right">{data.advance}</td>
                    </tr>
                <tr>
                    <th>TOTAL EARNINGS Rs.</th>
                    <th className="text-right">
                    {
                        parseInt(data.basic_salary)+ parseInt(data.hra)+ parseInt(data.medical_allowance)+ parseInt(data.transportation_allowance)+ parseInt(data.bonus)
                    }
                    </th>
                    <th></th>
                    <th className="text-right">TOTAL DEDUCTION Rs.</th>
                    <th className="text-right">
                        { parseInt(data.empr_pf_contri)+ parseInt(data.emp_pf_contri)+ parseInt(data.esi_of_empr_contri)+ parseInt(data.esi_of_emp_contri)+parseInt(data.tds)+parseInt(data.advance)+parseInt(data.absent_short_time)}
                    </th>
                    </tr>
                <tr>
                    <td>Arrear/ Incentives</td>
                    <td className="text-right">0</td>
                    <th></th>
                    <th></th>
                    <th></th>
                    </tr>
                <tr>
                    <th>Net Pay Rs.=></th>
                    <th>{data.total_paid}</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </Table>
            {key === 2 ?<div className="page-break"></div>:null}
            </div>
             )
            })}
            </div>
      )

    if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col lg={9}>
                        <Card>
                        <CardHeader>
                            <h4>Generate Report</h4>
                        </CardHeader>
                        <CardBody>
                            <Form >
                            <Table borderless >
                               <tr>
                                   <th>Employee</th>
                                   <td>
                                       <select name="emp_id" onChange={this.myChangeHandler} className="p-1">
                                           <option value="0">Please Select employee</option> 
                                           <option value="all">All</option>
                                           {this.state.employees.map((emp,key)=>{
                                              return <option value={emp.id}>{emp.name}</option>
                                           })}
                                               </select> 
                                               </td>
                               </tr>
                               <tr>
                                   <th>From Month</th>
                                   <td><DatePicker
                                    selected={this.state.startDateFrom}
                                    selectsStart
                                    startDate={this.state.startDateFrom}
                                    endDate={this.state.endDate}
                                    dateFormat="MMMM, yyyy"
                                    showMonthYearPicker
                                    onChange={this.handleChangeStartFrom} className="p-1"/>  </td>
                               </tr>
                               <tr>
                                   <th>To Month</th>
                                   <td> <DatePicker
                                    selected={this.state.startDateTo}
                                    selectsStart
                                    startDate={this.state.startDateTo}
                                    endDate={this.state.endDate}
                                    dateFormat="MMMM, yyyy"
                                    showMonthYearPicker
                                    onChange={this.handleChangeStartTo} className="p-1"/>  </td>
                               </tr>
                            </Table>
                            <Button color="primary" type="button" onClick={this.generateReport}>Submit</Button>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
                <div style={{display:"none"}}>
                     {PDF}
                   </div>
            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default SalaryReport;