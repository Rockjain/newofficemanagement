import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Finalreport from './report';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><Finalreport /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});