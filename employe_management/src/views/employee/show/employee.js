import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button } from 'reactstrap';
import axios from 'axios';
import { Link } from 'react-router-dom';
import config from '../../../port.js';

class Employee extends Component{
    constructor(props){
        super(props); 
        this.state = ({employee:[],preivious_experience:[],bankdetail:[],experience_table:true,bank_table:true})
        console.log(this.props.match.params.id)
      }

    componentDidMount () {
        let id = this.props.match.params.id;
        axios.get(config.port+"/users/getEmployeeById",{
            params:{id:id}
        }).then(res=>{
            
          console.log("data",res.data)
          this.setState({employee:res.data.data,preivious_experience:res.data.preivious_experience,bankdetail:res.data.account_info})
          console.log(this.state.bankdetail)
          if(this.state.preivious_experience[0].companyname !== ""){
              this.setState({experience_table:false})
          }else{}

          if(this.state.bankdetail !== ""){
              this.setState({bank_table:false})
          }else{}
        })
    }

    render(){
        
        const employeeDetail=this.state.employee.map((emp,key)=>{
            return(
                <div>
                    <tr>
                        <td style={{width:"200px"}}>Id</td><td >{emp.id}</td>
                    </tr>
                    <tr>
                        <td>Name</td><td>{emp.name}</td>
                    </tr>
                    <tr>
                        <td>Father's Name</td><td>{emp.father_name}</td>
                    </tr>
                    <tr>
                        <td>Date Of Birth</td><td>{emp.dob}</td>
                    </tr>
                    <tr>
                        <td>Gender</td><td>{emp.gender}</td>
                    </tr>
                    <tr>
                        <td>Local Address</td>
                        <td>{emp.local_address_l1}, {emp.local_address_l2} {emp.local_city} {emp.local_state} {emp.local_country} {emp.local_pincode} </td>
                    </tr>
                    <tr>
                        <td>Parmanent Address</td>
                        {emp.parmanent_address_l1 === "" ?<td>Same As Local Address</td>:<td>{emp.parmanent_address_l1} {emp.parmanent_address_l2} {emp.parmanent_city} {emp.parmanent_state} {emp.parmanent_country} {emp.parmanent_pincode} </td>}
                    </tr>
                    <tr>
                        <td>Pancard Number</td><td>{emp.pan_card_no}</td>
                    </tr>
                    <tr>
                        <td>Adharcard number</td><td>{emp.adhar_card_no}</td>
                    </tr>
                    <tr>
                        <td>Employee Code</td><td>{emp.emp_code}</td>
                    </tr>
                    <tr>
                        <td>Joining Date</td><td>{emp.joining_date}</td>
                    </tr>
                    <tr>
                        <td>Releiving Date</td><td>{emp.releiving_date}</td>
                    </tr>
                    <tr>
                        <td>preivious_experience</td><td>
                       
                        <Table hidden={this.state.experience_table} size="sm" bordered>
                                    <tr>
                                        <th>S.No</th>
                                        <th>company Name</th>
                                        <th>Joining Date</th>
                                        <th>Releiving Date</th>
                                        <th>Designation</th>
                                    </tr>
                        {this.state.preivious_experience.map((exp,key)=>{
                            return(
                               
                                    <tr>
                                    <td>{key+1}</td>
                                    <td>{exp.companyname}</td>
                                    <td>{exp.joiningdate}</td>
                                    <td>{exp.releivingdate}</td>
                                    <td>{exp.designation}</td>
                                    </tr>
                               
                            )
                        })}
                        </Table>
                        </td>
                    </tr>
                   
                    <tr>
                        <td>Department</td><td>{emp.department}</td>
                    </tr>
                    <tr>
                        <td>Bank Details</td><td>
                        <Table hidden={this.state.bank_table} size="sm" bordered>
                            <tr>
                                <th>Account Name</th>
                                <th>Account Number</th>
                                <th>Bank Name</th>
                                <th>Branch Name</th>
                                <th>Ifsc Code</th>
                            </tr>
                            <tr>
                        {this.state.bankdetail.map((bank,key)=>{
                            return(
                               
                                    
                                        <td>{bank}</td>
                                   
                               
                            )
                        })}
                         </tr>
                         </Table>
                        </td>
                    </tr>
                    <tr>
                        <td>Status</td><td>{emp.status}</td>
                    </tr>
                </div>
            )
        })
           
       let editLink = `/employee/edit/${this.props.match.params.id}`
        return (
          <div className="animated fadeIn">
            <Row>
              <Col lg={9}>
                <Card>
                  <CardHeader>
                    <strong>
                        <i class="fa fa-user pr-1"></i>Employee id: {this.props.match.params.id}
                        <Link to={editLink}><Button outline color="secondary" className=" float-right"><i className="fa fa-pencil text-dark"></i></Button></Link>
                    </strong>
                  </CardHeader>
                  <CardBody>
                      <Table responsive hover >
                        <tbody >
                        {employeeDetail}
                        </tbody>
                      </Table>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        )
    }
}

export default Employee;