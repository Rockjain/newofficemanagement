/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import {Badge, Col, Row,Table, Card, CardBody, CardHeader,Button,InputGroup, InputGroupAddon,Input} from 'reactstrap';
import config from '../../../port.js';
import { confirmAlert } from 'react-confirm-alert'; 
import 'react-confirm-alert/src/react-confirm-alert.css'; 
import axios from 'axios';
import Pagination from "react-js-pagination";

class ShowEmployee extends Component{
     constructor(props){
    super(props);
    this.state = ({employee:[],employe:[],add:false,delete:true,activePage: 1,
      pages:4,pageSize:10 ,pageNumber:0,currentIndex:1,pagesIndex:[],pageStart:1,inputNumber:""
    });
  }

componentDidMount () {
      axios.get(config.port+"/users/showemployee").then(res=>{
        console.log("data",res.data)
        this.setState({employee:res.data,employe:res.data})
        console.log(this.state.employee)

        let pages;
               
                let pageNumber = parseInt(""+ (this.state.employee.length / this.state.pageSize));
                    if(this.state.employee.length % this.state.pageSize !== 0){
                        pageNumber ++;
                        this.setState({pageNumber:pageNumber}) 
                    }
                    
                    if(pageNumber  < this.state.pages){
                         pages =  pageNumber;
                         this.setState({pages:pages}) 
                    }
                    console.log("pages",pageNumber,pages) 
                       
                   this.refreshItems();
                }) 

 }

fillArray=()=>{
        var obj = [];
        for(var index = this.state.pageStart; index< this.state.pageStart + this.state.pages; index ++) {
                    obj.push(index);
        }
        console.log("obj",obj)
        return obj;
        
  }
    
refreshItems=()=>{
        let items = this.state.employee.slice((this.state.currentIndex - 1)*this.state.pageSize, (this.state.currentIndex) * this.state.pageSize);
        let pagesIndex =  this.fillArray();
        //console.log(this.items)
        this.setState({employe:items,pagesIndex:pagesIndex})
        //console.log("item",this.state.items)
  }

      
handlePageChange=(pageNumber)=>{
      console.log(`active page is ${pageNumber}`);
      this.setState({activePage:pageNumber,currentIndex:pageNumber});
     this.refreshItems();
  }

myChangeHandler = (event) => {
      let val = event.target.value;
      this.setState({pageSize: val,currentIndex:1});
      console.log(this.state.pageSize)
      this.refreshItems();
  }

onchangecheckbox=()=>{
    var ref = document.getElementById("employee")
        let checkemp=0
        var getemployee = ref.getElementsByClassName('d-check')
        for(let i=0;i<getemployee.length;i++){
            
            if(getemployee[i].checked === true){
                checkemp++
                
              }
             
        }
        console.log(checkemp)
        if(checkemp >0){
            this.setState({add:true,delete:false}) 
        }else{
            this.setState({add:false,delete:true})
        }
  }

confirmdelete=(id)=>{
  
   
    axios.post(config.port+"/users/deleteEmployee",{id:id}).then(res=>{
       if(res.data.success === true){
        axios.get(config.port+"/users/showemployee").then(res=>{
          console.log("data",res.data)
          this.setState({employee:res.data,employe:res.data})
          console.log(this.state.employee)
        })
        }
    })
}

delete=(id,name)=>{
    confirmAlert({
      title: 'Employee id:' + id,
      message: 'You Really Want To Delete Employee: ' + name,
      buttons: [
        {
          label: 'Yes',
          onClick: () => this.confirmdelete(id)
        },
        {
          label: 'No',
          
        }
      ]
    });
  }


multipledelete=()=>{
    var ref = document.getElementById("employee");
    let ids=[];
    var getemployee = ref.getElementsByClassName('d-check')
    for(let i=0;i<getemployee.length;i++){
        
        if(getemployee[i].checked === true){
            let id={id:getemployee[i].id}
            ids.push(id);
          }
         
    }
    console.log(ids)
    let eid = JSON.stringify(ids)
    axios.post(config.port+"/users/deletemultipleEmployee",{ids:eid}).then(res=>{
      console.log(res.data)
        if(res.data.success === true){

            for(let i=0;i<getemployee.length;i++){
                getemployee[i].checked = false  
            }

            this.setState({add:false,delete:true})

            axios.get(config.port+"/users/showemployee").then(res=>{
              console.log("data",res.data)
              this.setState({employee:res.data,employe:res.data})
              console.log(this.state.employee)
            })
        }
    })
}

search=(event)=>{
    let val = event.target.value;
    var queryResult=[];

    if(val !== ""){
    axios.get(config.port+"/users/searchemp",{params:{val:val}}).then(res=>{
   // console.log(res.data);
        res.data.map((rows,index)=>{ 
            queryResult.push(rows);
            console.log("query result ",queryResult);
        })
        this.setState({employe:queryResult})
        })
      }else{
        this.refreshItems();
    }
    
  }



render(){

     const paginationContent = (
        <div className="ml-5">
          <Pagination
            hideDisabled
            prevPageText='prev'
            nextPageText='next'
            firstPageText='first'
            lastPageText='last'
            activePage={this.state.activePage}
            itemsCountPerPage={this.state.pageSize}
            totalItemsCount={this.state.employee.length}
            itemClass="page-item"
            linkClass="page-link"
            onChange={this.handlePageChange}
            />
          </div>
      )

      var pagination;
      if(this.state.employee.length > this.state.pageSize){
        pagination = <div>{paginationContent}</div>
      }else{
        pagination = <div></div>
      }
        

      const getBadge = (status) => {
        return status === 'Current employee' ? 'success' :
          status === 'Ex employee' ? 'secondary' :
            status === 'Retired employee' ? 'warning' :
              status === 'Banned' ? 'danger' :
                'primary'
            }
    
     const Capitalize=(str)=>{
        return str.charAt(0).toUpperCase() + str.slice(1);
        }
        
      const EmployeeList = this.state.employe
      if(localStorage.getItem("token")){
      return(
            <div className="animated fadeIn">
            <Row>
              <Col xl={9}>
                  <Card>
                    <CardHeader>
                        <h4><i className="fa fa-users"></i>  Employees 
                          <Link to='/addemployee'><Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} hidden={this.state.add} >
                            <i className="fa fa-plus" style={{color:"black"}}></i></Button></Link>
                            <a className="btn btn-danger btn-sm ml-2 float-right" hidden={this.state.delete} onClick={this.multipledelete}><i className="fa fa-trash" style={{color:"white"}} ></i></a>
                            </h4>
                      </CardHeader>
                    <CardBody>
                        <form id="employee">
                            <div>
                                <InputGroup className="mb-2">
                                  <InputGroupAddon addonType="prepend"><Button><i className="fa fa-search"></i></Button></InputGroupAddon>
                                  <Input placeholder="Search" className="w-75" name="searchbar" onChange={this.search}/> 
                                   <Input type="select"  name="pageSize" id="pageSize" onChange={this.myChangeHandler} onMouseOut={this.myChangeHandler} >
                                                                    <option value="10">10</option>
                                                                    {this.state.employee.length>10?<option value="15">15</option>:<span> </span>}
                                                                    {this.state.employee.length>15?<option value="20">20</option>:<span> </span>}
                                                                    {this.state.employee.length>20?<option value="25">25</option>:<span> </span>}
                                                                    {this.state.employee.length>25?<option value="30">30</option>:<span> </span>}
                                                                    </Input>
                                                
                                    </InputGroup>
                              </div>
                            <Table responsive hover>
                                <thead>
                                  <tr>
                                      <th scope="col">S.No</th>
                                      <th scope="col">Name</th>
                                      <th scope="col">Department</th>
                                      <th scope="col">Gender</th>
                                      <th scope="col">Status</th>
                                      <th scope="col">Action</th>
                                    
                                    </tr>
                                </thead>
                                <tbody>
                                  {EmployeeList.map((employee, index) =>{
                                    const employeeLink = `/employee/show/${employee.id}`
                                    const editLink = `employee/edit/${employee.id}` 
                                    
                                    return(
                                        <tr key={employee.id.toString()}>

                                          <th scope="row">

                                            <input type="checkbox" className="mr-2 d-check" value={employee.id} id={employee.id}  onChange={this.onchangecheckbox}/>

                                            <Link to={employeeLink}>{index+1}</Link>
                                            </th>
                                          <td><Link to={employeeLink}>{Capitalize(employee.name)}</Link></td>
                                          <td>{Capitalize(employee.department)}</td>
                                          <td>{Capitalize(employee.gender)}</td>
                                          <td><Link to={employeeLink}><Badge color={getBadge(Capitalize(employee.status))}>{Capitalize(employee.status)}</Badge></Link></td>

                                          <td>
                                              <Link to={editLink}><Button className="btn-sm" color="primary"><i className="fa fa-pencil" style={{color:"white"}}></i></Button></Link>

                                              <a className="btn btn-danger btn-sm ml-2"  onClick={()=>this.delete(employee.id, employee.name)}><i className="fa fa-trash" style={{color:"white"}}></i></a>
                                            
                                              </td>
                                      </tr>

                                      )
                                    })}
                                </tbody>
                              </Table>
                              {pagination}
                          </form>
                      </CardBody>
                </Card>
                </Col>
            </Row>
          </div>
        
        )
      }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
            
  }
}

export default ShowEmployee;