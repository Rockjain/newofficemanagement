import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import AddEmployee from './addemployee';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><AddEmployee /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});
