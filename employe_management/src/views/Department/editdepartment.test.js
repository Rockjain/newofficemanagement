import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import { mount } from 'enzyme'
import EditDepartment from './editdepartment';


it('renders without crashing', () => {
  const wrapper = mount(
    <MemoryRouter> 
      <EditDepartment match={{params: {id: "1"}, isExact: true, path: "/editdepartment/:id", name: "Edit Department"}}/>
    </MemoryRouter>
  );
  expect(wrapper.containsMatchingElement(<strong>Department</strong>)).toEqual(true)
  wrapper.unmount()
});