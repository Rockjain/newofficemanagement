import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Department from './departments';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><Department /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});