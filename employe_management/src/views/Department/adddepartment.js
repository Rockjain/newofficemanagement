/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {Component} from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Col, Row,Table, Card, CardBody, CardHeader,} from 'reactstrap';
import axios from 'axios';
import config from '../../port.js';

class AddDepartment extends Component{
      constructor(props){
        super(props)
        this.state = ({department:"",parent_id:"", maindepartment:[],redirect:false})
      
    }

    componentDidMount(){
        axios.get(config.port+"/users/department").then(res=>{
            let maindepartment=[];
            res.data.map((data,index)=>{
               
                    if(data.parent_id === 0){
                        maindepartment.push(data)
                    }
                
            })
            this.setState({maindepartment:maindepartment})
            console.log(maindepartment)
        })
    }

    myChangeHandler = (event) => {
       
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
      
      //console.log(this.state)
    }

    addDepartment=(event)=>{
        if(this.state.department === ""){
            alert("Please fill department Field")
        }else{
        event.preventDefault();
        //alert(this.state.department)
       // alert(this.state.parent_id)
        axios.post(config.port+"/users/adddepartment",{department:this.state.department,parent_id:this.state.parent_id})
        .then(res=>{
            console.log(res.data)
            if(res.data.success === true){
                axios.get(config.port+"/users/department").then(res=>{
                    let maindepartment=[];
                    res.data.map((data,index)=>{
                       
                            if(data.parent_id === 0){
                                maindepartment.push(data)
                            }
                        
                    })
                    this.setState({maindepartment:maindepartment})
                    console.log(maindepartment)
                })
            }
        })
        this.setState({department:""})
        }
    }

    saveDepartment=(event)=>{
        if(this.state.department === ""){
            alert("Please fill department Field")
        }else{
        event.preventDefault();
        //alert(this.state.department)
       // alert(this.state.parent_id)
        axios.post(config.port+"/users/adddepartment",{department:this.state.department,parent_id:this.state.parent_id})
        this.setState({department:"",redirect:true})
        }
    }

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/department' />
        }
      }

    render(){
        return(
            <div>
                 
                 <Row>
                    <Col xl={6}>
                        <Card>
                        <CardHeader>
                            <h4>
                                <i className="fa fa-plus-circle"></i>
                                &nbsp; Add Departments</h4>
                        </CardHeader>
                        <CardBody>
                            <form >
                            <Table borderless>
                                <tbody>
                                <tr>
                                    <td>
                                        Department Name
                                    </td>
                                    <td>
                                       <input type="text" name="department" className="w-75 my-1" value={this.state.department} onChange={this.myChangeHandler} required/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       Main department
                                    </td>
                                    <td>
                                       <select name="parent_id" id="parent_id" className="w-75 my-1" onChange={this.myChangeHandler}>
                                            <option value="0">Main Department</option>
                                            {this.state.maindepartment.map((data,key)=>{
                                                return <option value={data.id} key={key}>{data.name}</option>
                                            })}
                                       </select>
                                    </td>
                                </tr>
                                </tbody>
                            </Table>
                            <button className="btn btn-success ml-3" type="submit" style={{float:"right"}} onClick={this.addDepartment}>Save & Add</button>

                            {this.renderRedirect()}

                            <button className="btn btn-primary ml-3" type="submit" style={{float:"right"}} onClick={this.saveDepartment}>Save & Exit</button>

                            <Link to="/department" className="text-white"><button className="btn btn-danger" type="button" style={{float:"right"}}>Cancel</button></Link>

                            
                            </form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default AddDepartment;