/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Col,  Row, Table, Card, CardBody, CardHeader,Button,InputGroup, InputGroupAddon,  Input} from 'reactstrap';
import axios from 'axios';
import Pagination from "react-js-pagination";
import config from '../../port.js';

class Department extends Component{
    constructor(props){
        super(props)
        this.state = ({department:[],subdepartment:[],maindepartment:[],delete:true,add:false,all_department:[],activePage: 1,pageSize:10,pageNumber:"",currentIndex:1,items:[]});

        this.handlePageChange = this.handlePageChange.bind(this);
       // this.refreshItems = this.refreshItems.bind(this);
       
    }
   

    
    refreshItems=()=>{
        let items = this.state.all_department.slice((this.state.currentIndex - 1)*this.state.pageSize, (this.state.currentIndex) * this.state.pageSize);
       // let pagesIndex =  this.fillArray();
        //console.log(this.items)
        this.setState({items:items})
        console.log("item",this.state.items,this.state.pageSize)
      }

    componentDidMount(){
            axios.get(config.port+"/users/subdepartment").then(res=>{
           
                this.setState({maindepartment:res.data})
             
                this.setState({all_department:res.data,department:res.data})
                
                   this.refreshItems();
                })
        
    }

    handlePageChange=(pageNumber)=>{
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage:pageNumber,currentIndex:pageNumber});
        console.log("index",this.state.currentIndex)
       this.refreshItems();
         
       var ref = document.getElementById("department")
                   
       var getdepartment = ref.getElementsByClassName('d-check')
       for(let i=0;i<getdepartment.length;i++){
          getdepartment[i].checked = false  
      }

      }

    myChangeHandler = (event) => {
        let val = event.target.value;
        console.log("val",val)
        this.setState({pageSize:val,currentIndex:1});
        this.refreshItems();
        console.log(this.state.pageSize)
    }


    editdepartment=(id)=>{
        console.log(id)
       
    }

    deletedepartment=(id)=>{
        axios.delete(config.port+"/users/deletedepartment",{params:{id:id}}).then(res=>{
            //console.log(res.data.success)
            if(res.data.success === true){
                axios.get(config.port+"/users/subdepartment").then(res=>{
           
                    this.setState({maindepartment:res.data})
                 
                    this.setState({all_department:res.data,department:res.data})
                    
                    this.refreshItems();

                    var ref = document.getElementById("department")
                   
                     var getdepartment = ref.getElementsByClassName('d-check')
                     for(let i=0;i<getdepartment.length;i++){
                        getdepartment[i].checked = false  
                    }
    
                    })     
            }
        })
    }

    checkeddelete=()=>{
        this.setState({add:true,delete:false})
    }

    onchangecheckbox=()=>{
        var ref = document.getElementById("department")
        let checkdepartment =0
        var getdepartment = ref.getElementsByClassName('d-check')
        for(let i=0;i<getdepartment.length;i++){
            
            if(getdepartment[i].checked === true){
                checkdepartment++
                
              }
             
        }
        if(checkdepartment >0){
            this.setState({add:true,delete:false}) 
        }else{
            this.setState({add:false,delete:true})
        }
    }

    multipledelete=()=>{
        var ref = document.getElementById("department")
       let ids=[];
        var getdepartment = ref.getElementsByClassName('d-check')
        for(let i=0;i<getdepartment.length;i++){
            
            if(getdepartment[i].checked === true){
                let id={id:getdepartment[i].id}
                ids.push(id);
              }
             
        }
        console.log(ids)
        let did = JSON.stringify(ids)
        axios.get(config.port+"/users/deletemultipledepartment",{params:{ids:did}}).then(res=>{
            if(res.data.success === true){
                for(let i=0;i<getdepartment.length;i++){
                    getdepartment[i].checked = false  
                }

                this.setState({add:false,delete:true})
                axios.get(config.port+"/users/subdepartment").then(res=>{
           
                    this.setState({maindepartment:res.data})
                 
                    this.setState({all_department:res.data,department:res.data})
                    
                       this.refreshItems();
                    })
            
            }
        })
    }

    search=(event)=>{
        
        let val = event.target.value;
        //console.log(nam,val)
        var queryResult=[];
        if(val !== ""){
        axios.get(config.port+"/users/searchdep",{params:{val:val}}).then(res=>{
      
            res.data.map((rows,index)=>{
            queryResult.push(rows);
            console.log("query result ",queryResult);
        })
        this.setState({items:queryResult})
        })
        }else{
            this.refreshItems();
        }
        
    }

    render(){

        const paginationContent = (
            <div className="ml-5">
             <Pagination
                hideDisabled
                prevPageText='prev'
                nextPageText='next'
                firstPageText='first'
                lastPageText='last'
                activePage={this.state.activePage}
                itemsCountPerPage={this.state.pageSize}
                totalItemsCount={this.state.department.length}
                itemClass="page-item"
                linkClass="page-link"
                onChange={this.handlePageChange}
                />
              </div>
          )
    
          var pagination;
          if(this.state.department.length > this.state.pageSize){
            pagination = <div>{paginationContent}</div>
          }else{
            pagination = <div></div>
          }
        if(localStorage.getItem("token")){
        if(this.state.department.length > 0){
            return(
                <div>
                    <Row>
                            <Col xl={9}>
                                    <Card>
                                            <CardHeader>
                                                    <h4>
                                                        <i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; Departments
                                                        
                                                         <Link to='/department/add'>

                                                        <Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} hidden={this.state.add}><i className="fa fa-plus" style={{color:"black"}}></i></Button></Link>

                                                    <a className="btn btn-danger btn-sm ml-2 float-right" hidden={this.state.delete} onClick={this.multipledelete}><i className="fa fa-trash" style={{color:"white"}} ></i></a></h4>
                                                </CardHeader>
                                            <CardBody>
                                                <form id="department">
                                                    <div>
                                                        <InputGroup className="mb-2">
                                                                <InputGroupAddon addonType="prepend"><Button><i className="fa fa-search"></i></Button></InputGroupAddon>
                                                                <Input placeholder="Search" className="w-75" name="searchbar" onChange={this.search}/> 
                                                                    
                                                                <Input type="select"  name="pageSize" id="pageSize" onChange={this.myChangeHandler} onMouseOut={this.myChangeHandler}>
                                                                    <option value="10">10</option>
                                                                    {this.state.all_department.length>10?<option value="15">15</option>:<span> </span>}
                                                                    {this.state.all_department.length>15?<option value="20">20</option>:<span> </span>}
                                                                    {this.state.all_department.length>20?<option value="25">25</option>:<span> </span>}
                                                                    {this.state.all_department.length>25?<option value="30">30</option>:<span> </span>}
                                                                    </Input>
                                                        
                                                            </InputGroup>
                                                        
                                                        </div>
                                                    <Table >
                                                        <tbody>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>Department</th>
                                                                <th>Parent Department</th>
                                                                <th>Action</th>
                                                            
                                                            </tr>
                                                            {this.state.items.map((data,key)=>{
                                                                const editlink = `department/${data.id}`
                                                                return(
                                                                    <tr key={key}>
                                                                            <td>
                                                                                <input type="checkbox" className="mr-2 d-check" value={data.id} id={data.id}  onChange={this.onchangecheckbox}/>
                                                                                {key+1}
                                                                                </td>
                                                                            <td>{data.name}</td>
                                                                            {data.parent_name === null ?<td>Main Department</td>: <td>{data.parent_name}</td>}
                                                                            <td><Link to={editlink}><a className="btn btn-primary btn-sm" onClick={()=>this.editdepartment(data.id)}><i className="fa fa-pencil" style={{color:"white"}}></i></a></Link><a className="btn btn-danger btn-sm ml-2" onClick={()=>this.deletedepartment(data.id)}><i className="fa fa-trash" style={{color:"white"}}></i></a></td>
                                                                        </tr>
                                                                )
                                                            })}
                                                        
                                                        </tbody> 
                                                    </Table>
                                                    <div className="ml-5">
                                                      {pagination}
                                                    </div>
                                                </form>
                                            
                                        </CardBody>
                                        </Card>
                                </Col>
                        </Row>
                </div>
            )
        }else{
            return(
                <div>
                    <Row>
                            <Col xl={9}>
                                    <Card>
                                            <CardHeader>
                                                <h4> Departments  <Link to='/department/add'><Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} hidden={this.state.add}>
                                                <i className="fa fa-plus" style={{color:"black"}}></i></Button></Link>
                                                </h4>
                                            </CardHeader>
                                        <CardBody>
                                                <h5>Please Click on Plus Button above to Add the Department</h5>
                                            </CardBody>
                                    </Card>
                                </Col>
                        </Row>
                    </div>
            ) 
        }
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
    }
}

export default Department;