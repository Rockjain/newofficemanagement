import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Table, Form, Button,Input,InputGroup, InputGroupAddon, InputGroupText} from 'reactstrap';
import Moment from  'react-moment';
import { Redirect } from 'react-router-dom'
import config from '../../port.js';


class EditRoles extends  Component{ 

    constructor(props){
        super(props); 
        // this.myCheckBox=this.myCheckBox.bind(this);
        this.state = ({admin:[], id:"", startDate:new Date(), role_name:"",clientsview:0,clientsadd:0,clientsedit:0,clientsdelete:0,projectsview:0,projectsadd:0,projectsedit:0,projectsdelete:0, tasksview:0,tasksadd:0,tasksedit:0,tasksdelete:0, task_workview:0,task_workadd:0,task_workedit:0,task_workdelete:0, usersview:0,usersadd:0,usersedit:0,usersdelete:0, employeesview:0,employeesadd:0,employeesedit:0,employeesdelete:0, salaryview:0,salaryadd:0,salaryedit:0,salarydelete:0, reportsview:0,reportsadd:0,reportsedit:0,reportsdelete:0, settingsview:0,settingsadd:0,settingsedit:0,settingsdelete:0, permission:"",redirect:0,tr:true, fl:0});
      }

      componentDidMount(){

        let id = this.props.match.params.id;
        console.log(id)
        axios.get(config.port+"/users/showrolesbyid",{params:{id:id}}).then(res=>{
            console.log("data",res.data)
            let data = res.data.modulename
            // console.log(data.tasksadd)
            this.setState({role_name:res.data.data[0].role_name,
                clientsview:data.clientsview,
                clientsadd:data.clientsadd,
                clientsedit:data.clientsedit,
                clientsdelete:data.clientsdelete,
                projectsview:data.projectsview,
                projectsadd:data.projectsadd,
                projectsedit:data.projectsedit,
                projectsdelete:data.projectsdelete,
                tasksview:data.tasksview,
                tasksadd:data.tasksadd,
                tasksedit:data.tasksedit,
                tasksdelete:data.tasksdelete,
                task_workview:data.task_workview,
                task_workadd:data.task_workadd,
                task_workedit:data.task_workedit,
                task_workdelete:data.task_workdelete,
                usersview:data.usersview, usersadd:data.usersadd,
                usersedit:data.usersedit, usersdelete:data.usersdelete,
                employeesview:data.employeesview, employeesadd:data.employeesadd,employeesedit:data.employeesedit,employeesdelete:data.employeesdelete,salaryview:data.salaryview,salaryadd:data.salaryadd,salaryedit:data.salaryedit,salarydelete:data.salarydelete,reportsview:data.reportsview,reportsadd:data.reportsadd,reportsedit:data.reportsedit,reportsdelete:data.reportsdelete,settingsview:data.settingsview, settingsadd:data.settingsadd, settingsedit:data.settingsedit,settingsdelete:data.settingsdelete})
         
            console.log(this.state)
          }) 
     
      }


      myCheckBox=(event)=>{
        let nam = event.target.name;
        let val = event.target.value;
        // console.log(nam,val,event)
        this.setState({[nam]: 1});
      }

      myCheckBox1=(event)=>{
        let nam = event.target.name;
        let val = event.target.value;
        // console.log(nam,val,event)
        this.setState({[nam]: 0});
      }

      myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
        console.log(this.state)
      }

      updateroles=(e) =>{
        e.preventDefault();
        console.log(this.state)
        axios.post(config.port+"/users/updateuserroles",{
                role_name:this.state.role_name,
                clientsview:this.state.clientsview,
                clientsadd:this.state.clientsadd,
                clientsedit:this.state.clientsedit,
                clientsdelete:this.state.clientsdelete,
                projectsview:this.state.projectsview,
                projectsadd:this.state.projectsadd,
                projectsedit:this.state.projectsedit,
                projectsdelete:this.state.projectsdelete,
                tasksview:this.state.tasksview,
                tasksadd:this.state.tasksadd,
                tasksedit:this.state.tasksedit,
                tasksdelete:this.state.tasksdelete,
                task_workview:this.state.task_workview,
                task_workadd:this.state.task_workadd,
                task_workedit:this.state.task_workedit,
                task_workdelete:this.state.task_workdelete,
                usersview:this.state.usersview,
                usersadd:this.state.usersadd,
                usersedit:this.state.usersedit,
                usersdelete:this.state.usersdelete,             
                employeesview:this.state.employeesview,
                employeesadd:this.state.employeesadd,
                employeesedit:this.state.employeesedit,
                employeesdelete:this.state.employeesdelete,
                salaryview:this.state.salaryview,
                salaryadd:this.state.salaryadd,
                salaryedit:this.state.salaryedit,
                salarydelete:this.state.salarydelete,
                reportsview:this.state.reportsview,
                reportsadd:this.state.reportsadd,
                reportsedit:this.state.reportsedit,
                reportsdelete:this.state.reportsdelete,
                settingsview:this.state.settingsview,
                settingsadd:this.state.settingsadd,
                settingsedit:this.state.settingsedit,
                settingsdelete:this.state.settingsdelete,
                id:this.props.match.params.id
        }).then(res=>{ 
            if(res.data.success === true){
                this.setState({redirect:true})
            }    
        })

}

renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to='/roles' />
    }
  }

    render(){

        if(localStorage.getItem("token")){
            return (
                <div className="animated fadeIn">
                    
                    <Row>
                        <Col>
                            <Card>
                            <CardHeader>
                                <h4>Edit User Roles
                                   </h4>
                            </CardHeader>
                            <CardBody>
                                <Form onSubmit={this.updateroles}>
                                <Table borderless>  
                                    <tbody>
                                         <tr>
                                             <th>Role Name :</th>                                           
                                            <td>
                                                <input type="text" name="role_name" className="w-50" onChange={this.myChangeHandler} value={this.state.role_name} placeholder="User Role Name" />    
                                            </td>     
                                         </tr>
                                    </tbody>                                 
                                </Table>
                            <Col lg={9}>
                        <Table responsive striped hover>
                                <thead>
                                      <tr> 
                                          <th scope="col">Module Name</th>
                                          <th scope="col">View</th>
                                          <th scope="col">Add</th> 
                                          <th scope="col">Edit</th>    
                                          <th scope="col">Delete</th>                             
                                        </tr>
                                </thead>  
                            <tbody>
                                <tr>
                                   <th>Clients</th>
                                    <td>{this.state.clientsview === 0 ?<input type="checkbox" name="clientsview" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="clientsview" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.clientsadd === 0 ?<input type="checkbox" name="clientsadd" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="clientsadd" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.clientsedit === 0 ?<input type="checkbox" name="clientsedit" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="clientsedit" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.clientsdelete=== 0?<input type="checkbox" name="clientsdelete" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="clientsdelete" onChange={this.myCheckBox1} checked/>}</td>

                                </tr>
                                <tr>
                                    <th>Projects</th>
                                    <td>{this.state.projectsview === 0?<input type="checkbox" name="projectsview" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="projectsview" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.projectsadd === 0?<input type="checkbox" name="projectsadd" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="projectsadd" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.projectsedit ===0?<input type="checkbox" name="projectsedit" value="" onChange={this.myCheckBox}/>:<input type="checkbox" name="projectsedit" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.projectsdelete ===0?<input type="checkbox" name="projectsdelete" value="" onChange={this.myCheckBox}/>:<input type="checkbox" name="projectsdelete" onChange={this.myCheckBox1} checked/>}</td>                   
  
                                </tr>
                                <tr>
                                    <th>Tasks</th>

                                    <td>{this.state.tasksview ===0?<input type="checkbox" name="tasksview" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="tasksview" onChange={this.myCheckBox1} checked/>}</td> 

                                    <td>{this.state.tasksadd ===0?<input type="checkbox" name="tasksadd" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="tasksadd" onChange={this.myCheckBox1} checked/>}</td> 

                                    <td>{this.state.tasksedit ===0?<input type="checkbox" name="tasksedit" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="tasksedit" onChange={this.myCheckBox1} checked/>}</td> 

                                    <td>{this.state.tasksdelete ===0?<input type="checkbox" name="tasksdelete" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="tasksdelete" onChange={this.myCheckBox1} checked/>}</td>
                                        
                                </tr>
                                <tr>
                                    <th>Task Work</th>

                                    <td>{this.state.task_workview ===0?<input type="checkbox" name="task_workview" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="task_workview" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.task_workadd ===0?<input type="checkbox" name="task_workadd" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="task_workadd" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.task_workedit ===0?<input type="checkbox" name="task_workedit" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="task_workedit" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.task_workdelete ===0?<input type="checkbox" name="task_workdelete" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="task_workdelete" onChange={this.myCheckBox1} checked/>}</td>
          
                                </tr>
                                <tr>
                                    <th>Users</th>

                                    <td>{this.state.usersview ===0?<input type="checkbox" name="usersview" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="usersview" onChange={this.myCheckBox1} checked/>}</td>
                                    
                                    <td>{this.state.usersadd ===0?<input type="checkbox" name="usersadd" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="usersadd" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.usersedit ===0?<input type="checkbox" name="usersedit" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="usersedit" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.usersdelete ===0?<input type="checkbox" name="usersdelete" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="usersdelete" onChange={this.myCheckBox1} checked/>}</td>           
                                </tr>
                                 <tr>
                                    <th>Employees</th>

                                    <td>{this.state.employeesview ===0?<input type="checkbox" name="employeesview" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="employeesview" onChange={this.myCheckBox1} checked/>}</td>  

                                    <td>{this.state.employeesadd ===0?<input type="checkbox" name="employeesadd" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="employeesadd" onChange={this.myCheckBox1} checked/>}</td>  

                                    <td>{this.state.employeesedit ===0?<input type="checkbox" name="employeesedit" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="employeesedit" onChange={this.myCheckBox1} checked/>}</td>  

                                    <td>{this.state.employeesdelete ===0?<input type="checkbox" name="employeesdelete" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="employeesdelete" onChange={this.myCheckBox1} checked/>}</td>       
                                </tr>
                                <tr>
                                    <th>Salary</th>

                                    <td>{this.state.salaryview ===0?<input type="checkbox" name="salaryview" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="salaryview" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.salaryadd ===0?<input type="checkbox" name="salaryadd" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="salaryadd" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.salaryedit ===0?<input type="checkbox" name="salaryedit" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="salaryedit" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.salarydelete ===0?<input type="checkbox" name="salarydelete" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="salarydelete" onChange={this.myCheckBox1} checked/>}</td>

                                </tr>
                                <tr>
                                    <th>Reports</th>
                                    <td>{this.state.reportsview ===0?<input type="checkbox" name="reportsview" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="reportsview" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.reportsadd ===0?<input type="checkbox" name="reportsadd" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="reportsadd" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.reportsedit ===0?<input type="checkbox" name="reportsedit" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="reportsedit" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.reportsdelete ===0?<input type="checkbox" name="reportsdelete" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="reportsdelete" onChange={this.myCheckBox1} checked/>}</td>
          
                                </tr>
                                <tr>
                                    <th>Settings</th>

                                    <td>{this.state.settingsview ===0?<input type="checkbox" name="settingsview" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="settingsview" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.settingsadd ===0?<input type="checkbox" name="settingsadd" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="settingsadd" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.settingsedit ===0?<input type="checkbox" name="settingsedit" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="settingsedit" onChange={this.myCheckBox1} checked/>}</td>

                                    <td>{this.state.settingsdelete ===0?<input type="checkbox" name="settingsdelete" value="1" onChange={this.myCheckBox}/>:<input type="checkbox" name="settingsdelete" onChange={this.myCheckBox1} checked/>}</td>               
                                 </tr>
                            </tbody>
                                </Table>
                                </Col>                                       
                                {this.renderRedirect()}
                                      <Button type="submit" color="primary" className="float-right">Update</Button>
                                </Form>
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
    
                </div>
            )
        }else{
            return(
                <div>
                   <Row>
                        <Col>
                            <Card>
                                <CardHeader>
                                    <h4>Message</h4>
                                </CardHeader>
                                <CardBody>
                                      Sorry you are not loggedin.                                   
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
              )
        }
       
    }
}

export default EditRoles;