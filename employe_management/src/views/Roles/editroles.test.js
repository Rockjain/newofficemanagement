import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import { mount } from 'enzyme'
import EditRoles from './editroles';


it('renders without crashing', () => {
  const wrapper = mount(
    <MemoryRouter> 
      <EditRoles match={{params: {id: "1"}, isExact: true, path: "/roles/edit/:id", name: "Edit Roles"}}/>
    </MemoryRouter>
  );
  expect(wrapper.containsMatchingElement(<strong>Roles</strong>)).toEqual(true)
  wrapper.unmount()
});