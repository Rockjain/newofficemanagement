import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Roles from './roles';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><Roles /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});