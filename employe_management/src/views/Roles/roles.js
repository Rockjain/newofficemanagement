import React, { Component } from 'react';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import {  Card, CardBody, CardHeader, Col, Row, Button, Table, Form} from 'reactstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import config from '../../port.js';


class Roles extends Component{

    constructor(props){
        super(props); 
        this.state = ({admin:[], id:"", add:false,delete:true,shown:[], startDate:new Date(), role_name:"", module_name:"", permission:"",redirect:false});
      }


      refreshItems=()=>{
        let items = this.state.admin.slice((this.state.currentIndex - 1)*this.state.pageSize, (this.state.currentIndex) * this.state.pageSize);
       // let pagesIndex =  this.fillArray();
        //console.log(this.items)
        this.setState({items:items})
        console.log("item",this.state.items,this.state.pageSize)
      }





      componentDidMount(){       
        axios.get(config.port+"/users/showroles").then(res=>{
            console.log("data",res.data)
            this.setState({admin:res.data})
            console.log(this.state.admin)
          })      
    }

    myChangeHandler = (event) => {
        let val = event.target.value;
        console.log("val",val)
        this.setState({pageSize:val,currentIndex:1});
        this.refreshItems();
        console.log(this.state.pageSize)
    }


    deleteUserRoles=(id)=>{
        axios.delete(config.port+"/users/deleteroles",{params:{id:id}}).then(res=>{
            console.log(res)
            if(res.data.success===true){
              axios.get(config.port+"/users/showroles").then(res=>{
                  console.log(res.data)
                  this.setState({admin:res.data})
                  this.refreshItems();
                    //   var ref = document.getElementById("admin")
                 
                    //   var getUserRoles = ref.getElementsByClassName('d-check')
                    //   for(let i=0;i<  getUserRoles.length;i++){
                    //     getUserRoles[i].checked = false  
                    //       }

                   })
            }
        })
    }


    render(){
        if(localStorage.getItem("token")){
            return (
                <div className="animated fadeIn">
                    
                    <Row>
                        <Col>
                            <Card>
                            <CardHeader>
                                <h4>User Roles
                                <Link to='/roles/add'><Button color="secondary" outline size="sm" type="submit" style={{float:"right"}}>
                                <i className="fa fa-plus" style={{color:"black"}}></i></Button></Link>                    
                                </h4>
                            </CardHeader>
                            <CardBody>
                                <form id="roles">
                                <Table responsive hover>
                                    <thead>
                                      <tr>
                                          
                                          <th scope="col">S.No</th>
                                          <th scope="col">Role Name</th>
                                          {/* <th scope="col">Module Name</th>  */}
                                          {/* <th scope="col">Permission</th>     */}
                                          <th scope="col">Action</th>                             
                                        </tr>
                                    </thead>   
                                    <tbody>
                                    {this.state.admin.map((admin, index)=>{
                                        const editLink=`roles/edit/${admin.id}`
                                      return(
                                          <tr key={index}>
                                              <td>{index+1}</td>
                                              <td>{admin.role_name}</td>
                                              {/* <td>{admin.module_name}</td> */}
                                              {/* <td>{admin.permission}</td> */}
                                              <td>
                                              <Link to={editLink}><Button className="btn-sm" color="primary"><i className="fa fa-pencil" style={{color:"white"}}></i></Button></Link>

                                              <a className="btn btn-danger btn-sm ml-2" onClick={()=>this.deleteUserRoles(admin.id)}><i className="fa fa-trash" style={{color:"white"}}></i></a>
                                              </td>

                                          </tr>
                                      )


                                    })}
                                       
                                    </tbody>                            
                                  </Table>   
                                                           
                              </form>
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
    
                </div>
            )
        }else{
            return(
                <div>
                   <Row>
                            <Col>
                                <Card>
                                  <CardHeader>
                                      <h4>Message</h4>
                                  </CardHeader>
                                  <CardBody>
                                      Sorry you are not loggedin.
                                    
                                </CardBody>
                                </Card>
                            </Col>
                        </Row>
                  </div>
              )
        }
        
    }


}

export default Roles;