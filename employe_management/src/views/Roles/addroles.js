import React, { Component } from 'react';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import {  Card, CardBody, CardHeader, Col, Row, Button, Table, Form} from 'reactstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import config from '../../port.js';




class AddRoles extends Component{
    constructor(props){
        super(props); 
                        this.state = ({admin:[], id:"", startDate:new Date(), role_name:"",clientsview:0,clientsadd:0,clientsedit:0,clientsdelete:0,projectsview:0,projectsadd:0,projectsedit:0,projectsdelete:0, tasksview:0,tasksadd:0,tasksedit:0,tasksdelete:0, task_workview:0,task_workadd:0,task_workedit:0,task_workdelete:0, usersview:0,usersadd:0,usersedit:0,usersdelete:0, employeesview:0,employeesadd:0,employeesedit:0,employeesdelete:0, salaryview:0,salaryadd:0,salaryedit:0,salarydelete:0, reportsview:0,reportsadd:0,reportsedit:0,reportsdelete:0, settingsview:0,settingsadd:0,settingsedit:0,settingsdelete:0, permission:"",redirect:false});
      }
 

    componentDidMount(){       
        axios.get(config.port+"/users/user").then(res=>{
            console.log("data",res.data)
            this.setState({admin:res.data})
            console.log(this.state.admin)
          })      
    }

    myChangeHandler = (event) => {
        console.log(event)
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
       console.log(this.state)

    }



    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/roles' />
        }
      }

    handleChange=(date)=>{
        this.setState({
          startDate: date
        });
    }

    handletimeChange=idx=>evt=>{
        const time = this.state.time.map((time, sidx) => {
            if (idx === sidx){
                return evt.target.value ;
                }else{
                    return time 
                }            
          });
         
          this.setState({time:time});
          console.log(this.state.time)
    }

      AddUserRoles=(event)=>{
        event.preventDefault();
        console.log(this.state)
        if(this.state.role_name === ""|| this.state.module_name === ""){
            alert("Please Select permission")
          }else{ 
            axios.post(config.port+"/users/adduserrole",{
                role_name:this.state.role_name,
                // permission:this.state.permission,
                clientsview:this.state.clientsview,
                clientsadd:this.state.clientsadd,
                clientsedit:this.state.clientsedit,
                clientsdelete:this.state.clientsdelete,
                projectsview:this.state.projectsview,
                projectsadd:this.state.projectsadd,
                projectsedit:this.state.projectsedit,
                projectsdelete:this.state.projectsdelete,
                tasksview:this.state.tasksview,
                tasksadd:this.state.tasksadd,
                tasksedit:this.state.tasksedit,
                tasksdelete:this.state.tasksdelete,
                task_workview:this.state.task_workview,
                task_workadd:this.state.task_workadd,
                task_workedit:this.state.task_workedit,
                task_workdelete:this.state.task_workdelete,
                usersview:this.state.usersview,
                usersadd:this.state.usersadd,
                usersedit:this.state.usersedit,
                usersdelete:this.state.usersdelete,             
                employeesview:this.state.employeesview,
                employeesadd:this.state.employeesadd,
                employeesedit:this.state.employeesedit,
                employeesdelete:this.state.employeesdelete,
                salaryview:this.state.salaryview,
                salaryadd:this.state.salaryadd,
                salaryedit:this.state.salaryedit,
                salarydelete:this.state.salarydelete,
                reportsview:this.state.reportsview,
                reportsadd:this.state.reportsadd,
                reportsedit:this.state.reportsedit,
                reportsdelete:this.state.reportsdelete,
                settingsview:this.state.settingsview,
                settingsadd:this.state.settingsadd,
                settingsedit:this.state.settingsedit,
                settingsdelete:this.state.settingsdelete,
                created_date:this.state.startDate
            }).then(res=>{
                if(res.data.success===true){
                    this.setState({redirect:true})
                }
            })  
        }              
    }

    render(){
        if(localStorage.getItem("token")){
          return (
              <div className="animated fadeIn">                 
                  <Row>
                      <Col>
                          <Card>
                          <CardHeader>
                              <h4>User Roles
                                    <span className="float-right"><DatePicker
                                      selected={this.state.startDate}
                                      onChange={this.handleChange}
                                      dateFormat="MMMM dd, yyyy"
                                      className=" pl-1 float-right"/> </span>
                              </h4>
                          </CardHeader>
                          <CardBody>
                          <Form onSubmit={this.AddUserRoles}>
                              <Table borderless>
                              <tbody>   
                                  <tr>
                                      <th>User Role Name :</th>
                                       <td>
                                         <input type="text" name="role_name" className="w-50" onChange={this.myChangeHandler} placeholder="User Role Name" />                                         
                                      </td>
                                  </tr> 
                                </tbody>
                              </Table>
                              <Col lg={9}>
                              <Table responsive striped hover>
                                <thead>
                                      <tr> 
                                          <th scope="col">Module Name</th>
                                          <th scope="col">View</th>
                                          <th scope="col">Add</th> 
                                          <th scope="col">Edit</th>    
                                          <th scope="col">Delete</th>                             
                                        </tr>
                                    </thead>  
                            <tbody>
                                <tr>
                                    <th>Clients</th>
                                        <td><input type="checkbox" name="clientsview" value="1" onChange={this.myChangeHandler} /></td>
                                        <td><input type="checkbox" name="clientsadd" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="clientsedit" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="clientsdelete" value="1" onChange={this.myChangeHandler}/></td>
                                </tr>
                                <tr>
                                    <th>Projects</th>
                                        <td><input type="checkbox" name="projectsview" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="projectsadd" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="projectsedit" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="projectsdelete" value="1" onChange={this.myChangeHandler}/></td>
                                </tr>
                                <tr>
                                    <th>Tasks</th>
                                        <td><input type="checkbox" name="tasksview" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="tasksadd" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="tasksedit" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="tasksdelete" value="1" onChange={this.myChangeHandler}/></td>
                                </tr>
                                <tr>
                                    <th>Task Work</th>
                                        <td><input type="checkbox" name="task_workview" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="task_workadd" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="task_workedit" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="task_workdelete" value="1" onChange={this.myChangeHandler}/></td>
                                </tr>
                                <tr>
                                    <th>Users</th>
                                        <td><input type="checkbox" name="usersview" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="usersadd" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="usersedit" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="usersdelete" value="1" onChange={this.myChangeHandler}/></td>
                                </tr>
                                 <tr>
                                    <th>Employees</th>
                                        <td><input type="checkbox" name="employeesview" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="employeesadd" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="employeesedit" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="employeesdelete" value="1" onChange={this.myChangeHandler}/></td>
                                </tr>
                                <tr>
                                    <th>Salary</th>
                                        <td><input type="checkbox" name="salaryview" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="salaryadd" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="salaryedit" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="salarydelete" value="1" onChange={this.myChangeHandler}/></td>
                                </tr>
                                <tr>
                                    <th>Reports</th>
                                        <td><input type="checkbox" name="reportsview" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="reportsadd" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="reportsedit" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox" name="reportsdelete" value="1" onChange={this.myChangeHandler}/></td>
                                </tr>
                                <tr>
                                    <th>Settings</th>
                                        <td><input type="checkbox"  name="settingsview" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox"  name="settingsadd" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox"  name="settingsedit" value="1" onChange={this.myChangeHandler}/></td>
                                        <td><input type="checkbox"  name="settingsdelete" value="1" onChange={this.myChangeHandler}/></td>
                                 </tr>

                                    </tbody>

                                </Table>
                                </Col>
                                    {this.renderRedirect()}
                                       <Button color="primary" className="float-right">Submit</Button>
                                </Form>
                          </CardBody>
                          </Card>
                      </Col>
                  </Row>
              </div>
          )
      }else{
          return(
              <div>
                 <Row>
                          <Col>
                              <Card>
                                <CardHeader>
                                    <h4>Message</h4>
                                </CardHeader>
                                <CardBody>
                                    Sorry you are not loggedin.
                                  
                              </CardBody>
                              </Card>
                          </Col>
                      </Row>
                </div>
            )
      }
  }

}


export default AddRoles;

