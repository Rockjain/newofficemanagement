import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import { mount } from 'enzyme'
import EditTaskManage from './edittaskmanage';


it('renders without crashing', () => {
  const wrapper = mount(
    <MemoryRouter> 
      <EditTaskManage match={{params: {id: "1"}, isExact: true, path: "/employee/edit/:id", name: "Edit Department"}}/>
    </MemoryRouter>
  );
  expect(wrapper.containsMatchingElement(<strong>Department</strong>)).toEqual(true)
  wrapper.unmount()
});