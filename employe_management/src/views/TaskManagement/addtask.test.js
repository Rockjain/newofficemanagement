import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import AddTaskManagement from './addtask';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><AddTaskManagement /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});