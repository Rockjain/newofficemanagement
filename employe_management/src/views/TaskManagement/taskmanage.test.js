import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Taskmanagement from './taskmanage';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><Taskmanagement /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});