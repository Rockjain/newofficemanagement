

import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalBody, ModalHeader, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Table} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom'
import Moment from 'react-moment';
import config from '../../port.js';
// import DataTable from 'react-editable-table';
// import EditableLabel from 'react-inline-editing';
// import Popup from "reactjs-popup";


class ShowTaskManage extends Component{
    constructor(props){ 
        super(props)
        this.state = ({employee:[],emp_id:"",task:[],project_management:[],project_id:"",task_management:[],tasks:[], task_id:"",image:"", pdf:"", status:"",discription:"",document:"",attachments:"",task_name:"",document:[],threaddate:"",threaddiscription:"",threadstatus:"",threadhours:"",task_threading:[],modal: false,imagesrc:"",images:[],taskThread:[], delete:true,edit:[],shown:[],div:true})
        this.toggle = this.toggle.bind(this);
    }


    componentDidMount(){ 
        let id = this.props.match.params.id;
        axios.get(config.port+"/users/showtaskmanagebyid",{params:{id:id}}).then(res=>{
            console.log(res.data)
            let doc = res.data.document.file
            let img = res.data.document.images
           
            let document=[];
            let images=[];
            for(var i in doc){
                document.push(doc[i])
            }
            for(var j in img){
                images.push(img[j])
            }
            let emp_id;
            let taskThreadid;
            res.data.rows.map(emp=>{
                emp_id = emp.emp_id
                taskThreadid =emp.id
                
        axios.get(config.port+"/users/showtask",{params:{id:id}}).then(res=>{
            console.log(res.data)
            this.setState({taskThread:res.data})
          
            let Ttask=[];
            let edit=[];
            let shown=[];
            res.data.map((data,ind)=>{
            Ttask.push(data.Ttask);
            edit.push(true)
            shown.push(false)
            })
            this.setState({Ttask:Ttask,edit:edit,shown:shown})
        }) 
            })
            console.log(document,emp_id,taskThreadid,id)
            this.setState({task:res.data.rows, document:document, discription:res.data.document.discription, images:images,emp_id:emp_id,task_id:taskThreadid})
        })


       
 }


 addtaskthread=(event)=>{
         event.preventDefault();  
         if(this.state.threaddate === "" || this.state.threaddescription === "" || this.state.threadstatus === "" ||this.state.threadhours === ""){
         alert("Please Select Date, Description,Status And Hours")
         }else{        
            axios.post(config.port+"/users/addtask",{
                emp_id:this.state.emp_id,
                task_id:this.state.task_id,
                date:this.state.threaddate,
                description:this.state.threaddescription,
                status:this.state.threadstatus,
                hours:this.state.threadhours
                               
            }).then(res=>{
                if(res.data.success===true){
                    this.setState({redirect:true,div:true})

                    
            let t_id = this.props.match.params.id
            axios.get(config.port+"/users/showtask",{params:{id:t_id}}).then(res=>{
                    console.log(res.data)
                    this.setState({taskThread:res.data})
                      
                        let Ttask=[];
                        let edit=[];
                        let shown=[];
                        res.data.map((data,ind)=>{
                        Ttask.push(data.Ttask);
                        edit.push(true)
                        shown.push(false)
                        })
                        this.setState({Ttask:Ttask,edit:edit,shown:shown})
                    })

                }
            })   
        }      
        
  }


 
 

  showDiv=()=>{
      this.setState({div:false})
  }

  deleteTaskThread=(id)=>{
    let t_id = this.props.match.params.id
    axios.delete(config.port+"/users/deletetask",{params:{id:id}}).then(res=>{
        console.log(res)
        if(res.data.success===true){
          axios.get(config.port+"/users/showtask",{params:{id:t_id}}).then(res=>{
              console.log(res.data)
              this.setState({taskThread:res.data})
           
               })
        }
    })
}


    toggle=(imagesrc)=>{
            this.setState(prevState => ({
              modal: !prevState.modal,imagesrc:imagesrc
            }));
    }   
    myChangeHandler = (event) => {
        let val = event.target.value;
        let name = event.target.name;
        this.setState({[name]:val})
        console.log(this.state)
    }

    // edittaskthread=(event)=>{ 
    //     let id = this.props.match.params.id;
    //     console.log("hello", id)
    //     axios.post(config.port+"/users/updateTaskThread",{

    //                 emp_id:this.state.emp_id,
    //                 task_id:this.state.task_id,
    //                 date:this.state.date,
    //                 hours:this.state.hours,
    //                 description:this.state.description,               
    //                 status:this.state.status,
    //                 id:id
          
    //     }).then(res=>{
    //         if(res.data.success === true){
    //           this.setState({redirect:true})
               
    //         }
    //     })
    // }


render(){
        const Capitalize=(str)=>{
   return str.charAt(0).toUpperCase() + str.slice(1);
   }

if(localStorage.getItem("token")){
       return (
           <div className="animated fadeIn">
               
               <Row>
                   <Col>
                       <Card>
                       <CardHeader>{this.state.task.map((task,key)=>{
                           return(
                            <h4>Task Name : {task.task_name}<Moment format="MMMM DD, YYYY" className="float-right">{task.date}</Moment></h4>
                           )                                                   
                       })}
                 <Link to={"/taskmanage/edit/"+this.props.match.params.id} style={{color:"black"}}>
                <i className="fa fa-pencil float-right"></i></Link>
                                               
                       </CardHeader>
                       <CardBody>
                           <div className="row">                            
                           <div className="col-md-12 col-lg-8">
                               <Table borderless striped>
                                   {this.state.task.map((task,key)=>{
                                       return(
                                           <tbody>
                                               <tr>
                                                   <th>Project Name</th>
                                                   <td>{task.project_name}</td>
                                               </tr>
                                               <tr>
                                                   <th>Employee Name</th>
                                                   <td>{task.emp_name} </td>
                                               </tr> 
                                              
                                               <tr>
                                                   <th>Description</th>
                                                   <td>{task.discription} </td>
                                               </tr>                                                      
                                               <tr>
                                                   <th>Status</th>
                                                   <td>{task.status} </td>
                                               </tr>
                                                   
                                               <tr>
                                                   <th>Document</th>
                                                   <td>
                                                   {this.state.document.map((data,key)=>{
                                                        let docsrc = `${config.port}/images/${data}`
                                                       return <a target="blank" href={docsrc}>{data +","} &nbsp;</a>
                                                   })}
                                           </td>
                                               </tr>
                                           </tbody>
                                       )
                                   })}
                               </Table>
                              </div>
                              <div className="col-md-12 col-lg-4">
                              {this.state.images.map((doc,key)=>{
                                   let imagesrc = `${config.port}/images/${doc}`
                                  return(
                                      <div>
                                       <img src={imagesrc} alt="" className="m-2" width="80%" onClick={()=>this.toggle(imagesrc)}/> 

                                           <Modal isOpen={this.state.modal} fade={false} toggle={this.toggle} size="lg" className="p-0" style={{backgroundColor:"tarnsparent!important"}}>
                                           
                                           <ModalBody >
                                           <button className="close" onClick={this.toggle}>&times;</button>
                                           <img src={this.state.imagesrc} alt="" width="100%" height="100%"/> 
                                           </ModalBody>
                                           </Modal>
                                   </div>
                                  )
                              })}
                                   
                              </div>
                              

                              </div>
                              </CardBody>
        
                       </Card>
                 
                   <Card>
                    
    <CardBody>
                <Form id="taskthread" onSubmit={this.addtaskthread}>
                <Button color="primary" className="float-right" onClick={this.showDiv}>Add</Button>
                
                    <Table>
                        <thead>
                            <tr >  
                                <th>Date</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Hours</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                          
                        <tbody>
                       
                             <tr hidden={this.state.div}>     
                                    <td><input type="date"  name="threaddate" onChange={this.myChangeHandler} placeholder="Date"  value={this.state.date}/></td>

                                    <td><input type="text"  name="threaddescription"onChange={this.myChangeHandler} placeholder="Description" value={this.state.description}/></td>

                                     <td>
                                        <select  name="threadstatus" id="status" className="w-50" onChange={this.myChangeHandler} placeholder="Status" value={this.state.status}required>
                                            <option value="0">please select Status:</option>
                                            <option value="notstarted">Not Started</option>
                                            <option value="inprogress">In progress</option>
                                            <option value="completed">Completed</option>
                                        </select>
                                    </td>

                                     <td><input type="number"  name="threadhours" onChange={this.myChangeHandler} placeholder="Hours" value={this.state.hours}/></td>

                                        <td>
                                            <Button color="primary" type="submit" className="float-right" onClick={this.addtaskthread} hidden={this.state.submit}>Submit</Button>
                                         </td>
                                 </tr> 
                                
                         {this.state.taskThread.map((taskthread, index)=>{
                             const editLink=`/taskmanage/edittaskthread/${taskthread.id}`
                             
                             return(

                                <tr>
                                 <td><Moment format="DD-MMM-YYYY">{taskthread.date}</Moment></td>
                                <td>{taskthread.description}</td>
                                <td>{taskthread.status}</td>
                                <td>{taskthread.hours}</td>
                                <td>
                                    <Link to={editLink} ><Button className="btn-sm" color="primary"><i className="fa fa-pencil" style={{color:"white"}}></i></Button></Link>
                                    <Button onClick={()=>this.deleteTaskThread(taskthread.id)} type="button" className="btn btn-danger btn-sm ml-2"><i className="fa fa-trash" style={{color:"white"}}></i></Button>
                                </td>

                                </tr>

                             )


                         })}
                        </tbody>
                                
                            </Table>
                            
                            
                        </Form>
        
            
            </CardBody>
        </Card>
            
        </Col>
            
    </Row>

           </div>
       )
   }else{
       return(
           <div>
              <Row>
                       <Col>
                           <Card>
                             <CardHeader>
                                 <h4>Message</h4>
                             </CardHeader>
                             <CardBody>
                                 Sorry you are not loggedin.
                               
                           </CardBody>
                           </Card>
                       </Col>
                   </Row>
             </div>
         )
   }
}

}


 
export default ShowTaskManage;