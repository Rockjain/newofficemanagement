import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Table, Form, Button,Input,InputGroup, InputGroupAddon, InputGroupText} from 'reactstrap';
import Moment from  'react-moment';
import { Redirect } from 'react-router-dom'
import config from '../../port.js';
import FormData from 'form-data';




class EditTaskManage extends  Component{
    constructor(props){
        super(props)
        this.state = ({employee:[],emp_id:"",project_management:[],project_id:"",task_management:[],task:"",task_id:"",image:"", pdf:"", status:"",discription:"",document:"",attachments:"",task_name:"",multerImage:"",file:[],redirect:false,images:[]})
        this.upload= this.upload.bind(this);
     
    }

    componentDidMount(){
        
        let id = this.props.match.params.id;
        axios.get(config.port+"/users/currentemp").then(res=>{
            //console.log(res.data)
            this.setState({employee:res.data})           
        })

        axios.get(config.port+"/users/project").then(res=>{
            //console.log(res.data);
            this.setState({project_management:res.data})
        })

        axios.get(config.port+"/users/showtaskmanagement").then(res=>{
            //console.log(res.data);
            this.setState({task_management:res.data})
        })

        axios.get(config.port+"/users/showtaskmanagebyid",{params:{id:id}}).then(res=>{
            console.log(res.data)
        let data=res.data.rows[0]
        let doc = res.data.document.file
        let img = res.data.document.images
        let document=[];
        let images=[];
        for(var i in doc){
            document.push(doc[i])
        }
        for(var j in img){
            images.push(img[j])
        }
        this.setState({project_id:data.project_id, emp_id:data.emp_id,discription:data.discription,status:data.status,task_name:data.task_name,file:document, images:images})

        })
    }


    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
        console.log(this.state)
      }

        upload=(e)=>{
       
            console.log(e.target.files[0].type)
            let document=[]; 
            let images=[];
            const formData = new FormData();
            this.state.file.map((data,key)=>{
                document.push(data)
            })
            this.state.images.map((data,key)=>{
                images.push(data)
            })
    
            for(var x = 0; x<e.target.files.length; x++) {
            formData.append('myImage',e.target.files[x]);
            if(e.target.files[x].type === "image/png" || e.target.files[x].type === "image/jpg" || e.target.files[x].type === "image/jpeg"){
                images.push(e.target.files[x].name)
            }
            else document.push(e.target.files[x].name)
            }    
            // console.log("images",document,images);
            this.setState({file:document,images:images})
            axios.post(config.port+"/users/uploadTaskDoc",formData,{}).then((res) => {
                console.log(res.statusText)
                }).catch((error) => {
            });
        }
    
        
    update=(e) =>{
            e.preventDefault();
            console.log(this.state)

            axios.post(config.port+"/users/updateTaskManagement",{
                project_id:this.state.project_id,
                emp_id:this.state.emp_id,
                discription:this.state.discription,
                status:this.state.status,
                task_name:this.state.task_name,
                file:this.state.file,
                images:this.state.images,
                id:this.props.match.params.id
            }).then(res=>{
                if(res.data.success === true){
                    this.setState({redirect:true})
                }    
            })

    }
   

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/taskmanage' />
        }
      }

    render(){

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Edit Task Management
                               </h4>
                        </CardHeader>
                        <CardBody>
                            <Form enctype = "multipart/form-data" onSubmit={this.update}>
                            <Table borderless>
                              
                                <tr>
                                    <th>Project Name :</th>                                           
                                        <td>
                                        <select name="project_id" id="project_id" className="w-50" onChange={this.myChangeHandler} value={this.state.project_id}>
                                          
                                            {
                                            this.state.project_management.map((project_management,key)=>{
                                             return(
                                                 <option value={project_management.id}>{project_management.name}</option>
                                                    )
                                                })}
                                            </select> 
                                        </td>

                                    
                                </tr>
                                        <tr>
                                            <th>Employee Name :</th>
                                            <td>

                                                <select name="emp_id" id="emp_id" onChange={this.myChangeHandler} className="w-50" value={this.state.emp_id}>                                                   
                                                    {
                                                    this.state.employee.map((employee,key)=>{
                                                        return <option value={employee.id}>{employee.name}</option>
                                                    })
                                                    }
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Task Name :</th>
                                            <td>
                                        <select name="task_id" id="task_id" onChange={this.myChangeHandler} className="w-50" value={this.state.task_id}>
                                           {
                                               this.state.task_management.map((task_management,key)=>{
                                                   return <option value={task_management.id}>{task_management.task_name}</option>
                                               })
                                           } 
                                        </select>
                                    </td>
                                        </tr>
                                        <tr>
                                            <th>Attachements :</th>
                                          <td>
                                            <input type="file" name="myImage" multiple onChange={this.upload}/>   
                                          </td>
                                        </tr>

                                        <tr>
                                            <th>Description : </th>
                                            <td>
                                            <Input type="textarea" name="discription" placeholder="discription" className="w-75" onChange={this.myChangeHandler} value={this.state.discription}/>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Status :</th>
                                         

                                            <td><select  name="status" id="status" className="w-50" onChange={this.myChangeHandler} value={this.state.status} required>
                                                <option value="0">Please Select Status</option>
                                                <option value="not_started">Not Started </option>
                                                <option value="in_progress">In Progress</option>
                                                <option value="completed">Completed</option>
                                                </select>
                                            </td>



                                        </tr>
                              <tr>
                                  <td colspan="2">
                                  {this.renderRedirect()}
                                  <Button type="submit" color="primary" className="float-right">Update</Button>
                                  </td>
                              </tr>
                            </Table>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}

}


export default EditTaskManage;