import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button,Modal, ModalBody, ModalHeader, Form, Table,Input,nputGroup, InputGroupAddon, InputGroupText} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import config from '../../port.js';
import Moment from 'react-moment';
import FormData from 'form-data';




class AddTaskManagement extends Component{
        constructor(props){
        super(props)
        this.state = ({task:[],employee:[],project_management:[],name:"",task_id:"",project_id:"",emp_id:"",image:"", pdf:"",discription:"", document:"", status:"",task_name:"",multerImage:"",file:[],redirect:false, images:[]})
        this.upload= this.upload.bind(this);
    }
    componentDidMount(){
        axios.get(config.port+"/users/currentemp").then(res=>{
          console.log(res.data)
          this.setState({employee:res.data})

        })

        axios.get(config.port+"/users/project").then(res=>{
            console.log(res.data)
            this.setState({project_management:res.data})
        })
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
       // console.log(this.state)
      }



      upload=(e)=>{
       
        console.log(e.target.files[0].type)
        let document=[]; 
        let images=[];
        const formData = new FormData();

        for(var x = 0; x<e.target.files.length; x++) {
        formData.append('myImage',e.target.files[x]);
        if(e.target.files[x].type === "image/png" || e.target.files[x].type === "image/jpg" || e.target.files[x].type === "image/jpeg"){
            images.push(e.target.files[x].name)
        }
        else document.push(e.target.files[x].name)
        }

        // console.log("images",document,images);
        this.setState({file:document,images:images})
        axios.post(config.port+"/users/uploadTaskDoc",formData,{}).then((res) => {
            console.log(res.statusText)
            }).catch((error) => {
        });
    }

     
      addtaskmanagement=(event)=>{
        event.preventDefault();
          console.log(this.state)
          if(this.state.emp_id === ""){
            alert("Please Select Employee Name")
          }else{         
                axios.post(config.port+"/users/addtaskmanagement",{
                    
                    project_id:this.state.project_id,
                    emp_id:this.state.emp_id,
                    file:this.state.file,
                    images:this.state.images,
                    discription:this.state.discription,                    
                    status:this.state.status,
                    task_name:this.state.task_name                    
                }).then(res=>{
                    if(res.data.success===true){
                        this.setState({redirect:true})
                    }
                })         
            }
      }

      renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/taskmanage' />
        }
      }

  
    render(){
        if(localStorage.getItem("token")){
            return (
                <div className="animated fadeIn">
                    
                    <Row>
                        <Col>
                            <Card>
                            <CardHeader>
                                <h4><i className="fa fa-tasks"></i>&nbsp; Add Task</h4>
                            </CardHeader>
                            <CardBody>
                                <Form onSubmit={this.addtaskmanagement} enctype = "multipart/form-data">
                               
                                <Table borderless>
                                    <tbody>
                                         
                                        <tr>
                                            <th>Project Name :</th>                                           
                                            <td><select name="project_id" id="project_id" className="p-1 w-50" onChange={this.myChangeHandler}>
                                            <option value="0">Please Select Project Name</option>
                                            {this.state.project_management.map((project_management,key)=>{
                                                return(
                                                    <option value={project_management.id}>{project_management.name}</option>
                                                    )
                                                })}
                                                </select> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Employee Name :</th>
                                            <td>

                                                <select name="emp_id" id="emp_id" onChange={this.myChangeHandler} className="w-50" value={this.state.emp_id}>
                                                    <option value="0">Please Select Employee Name</option>
                                                    {
                                                    this.state.employee.map((employee,key)=>{
                                                        return <option value={employee.id}>{employee.name}</option>
                                                    })
                                                    }
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Task Name :</th>
                                            <td><input type="text" name="task_name" className="w-50" value={this.state.task_name} placeholder="Task Name" onChange={this.myChangeHandler}/></td>
                                        </tr>
    
                                        
                                        <tr>
                                            <th>Attachements :</th>
                                          <td>
                                            <input type="file" name="myImage" multiple onChange={this.upload}/>   
                                          </td>
                                        </tr>     

                                        <tr>
                                            <th>Description : </th>
                                            <td>
                                            <Input type="textarea" name="discription" placeholder="discription" className="w-75" onChange={this.myChangeHandler}/>
                                                
                                            </td>
                                        </tr>                                                                          
                                        <tr>
                                            <th>Status :</th>
                                         

                                            <td><select  name="status" id="status" className="w-50" onChange={this.myChangeHandler} required>
                                                <option value="0">Please Select Status</option>
                                                <option value="not_started">Not Started </option>
                                                <option value="in_progress">In Progress</option>
                                                <option value="completed">Completed</option>
                                                </select>
                                            </td>



                                        </tr>
                                        
    
                                    </tbody>
                                </Table>
                                {this.renderRedirect()}
                                     <Button color="primary" className="float-right">Submit</Button>
                                </Form>
                              </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            )            
  
           }else{ 
            return(
                <div>
                    <Row>
                        <Col>
                             <Card>
                                <CardHeader>
                                    <h4>Message</h4>
                                  </CardHeader>
                                  <CardBody>
                                      Sorry you are not loggedin.                  
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
              )
           }
        
    }
}

export default AddTaskManagement;