import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import { mount } from 'enzyme'
import ShowTaskManage from './showtaskmanage';


it('renders without crashing', () => {
  const wrapper = mount(
    <MemoryRouter> 
      <ShowTaskManage match={{params: {id: "1"}, isExact: true, path: "/taskmanage/show/:id", name: "Show Task Management"}}/>
    </MemoryRouter>
  );
  expect(wrapper.containsMatchingElement(<strong>Project</strong>)).toEqual(true)
  wrapper.unmount()
}); 