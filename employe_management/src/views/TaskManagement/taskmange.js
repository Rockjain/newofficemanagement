import React, { Component } from 'react';
import axios from 'axios';
import {Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalBody, ModalHeader, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Table, Badge} from 'reactstrap';
import { Link } from 'react-router-dom'
import Moment from 'react-moment';
import Pagination from 'react-js-pagination';
import { AppSwitch } from '@coreui/react';
import config from '../../port.js';
import inProgress from '../../assets/images/progress.png'
import completed from '../../assets/images/completed.png'
import notStarted from '../../assets/images/not-started.png'


class TaskManagement extends Component{
    constructor(props){
        super(props)
        this.state = 
        ({task:[],add:false,delete:true,shown:[],checked:false,startDate:new Date(),activePage: 1,pageSize:10,pageNumber:"",currentIndex:1,items:[],})  
        
    }

    // port = "http://localhost:3002"

    refreshItems=()=>{
        let items = this.state.task.slice((this.state.currentIndex - 1)*this.state.pageSize, (this.state.currentIndex) * this.state.pageSize);
       // let pagesIndex =  this.fillArray();
        //console.log(this.items)
        this.setState({items:items})
        console.log("item",this.state.items,this.state.pageSize)
      }

    componentDidMount() { 
        axios.get(config.port+"/users/showtaskmanagement").then(res=>{
            console.log(res.data)
            this.setState({task:res.data})
            this.refreshItems();
        })       
    }

    handlePageChange=(pageNumber)=>{
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage:pageNumber,currentIndex:pageNumber});
        console.log("index",this.state.currentIndex)
       this.refreshItems();

       var ref = document.getElementById("task")
                   
       var getTaskManagement = ref.getElementsByClassName('d-check')
       for(let i=0;i<  getTaskManagement.length;i++){
        getTaskManagement[i].checked = false  
        }

      }

      myChangeHandler = (event) => {
        let val = event.target.value;
        console.log("val",val)
        this.setState({pageSize:val,currentIndex:1});
        this.refreshItems();
        console.log(this.state.pageSize)
    }


    deleteTaskManagement=(id)=>{
          axios.delete(config.port+"/users/deleteTaskManagement",{params:{id:id}}).then(res=>{
              console.log(res)
              if(res.data.success===true){
                axios.get(config.port+"/users/showtaskmanagement").then(res=>{
                    console.log(res.data)
                    this.setState({task:res.data})
                    this.refreshItems();
                    var ref = document.getElementById("task")
                   
                        var getTaskManagement = ref.getElementsByClassName('d-check')
                        for(let i=0;i<  getTaskManagement.length;i++){
                            getTaskManagement[i].checked = false  
                            }

                     })
              }
          })
      }

      multipledelete=()=>{
        var ref = document.getElementById("task")
        let ids=[];
         var getTaskManagement = ref.getElementsByClassName('d-check')
         for(let i=0;i<getTaskManagement.length;i++){
             
             if(getTaskManagement[i].checked === true){
                 let id={id:getTaskManagement[i].id}
                 ids.push(id);
               }             
         }
         console.log(ids)
         let tid = JSON.stringify(ids)
         axios.delete(config.port+"/users/deletemultipletaskmanagement",{params:{ids:tid}}).then(res=>{
             if(res.data.success === true){
                 for(let i=0;i<getTaskManagement.length;i++){
                    getTaskManagement[i].checked = false  
                 }
                 this.setState({add:false,delete:true})
                 axios.get(config.port+"/users/showtaskmanagement").then(res=>{
                    console.log(res.data)
                    this.setState({task:res.data})
                })              
             }
         })
    }

    handleCheck=(task_id,id)=>{
        axios.post(config.port+"/users/toggleStatus",{id:id,task_id:task_id}).then(res=>{
            console.log(res.data)
            if(res.data.success===true){
                axios.get(config.port+"/users/showtaskmanagement").then(res=>{
                    console.log(res.data)
                    this.setState({task:res.data})
                    this.refreshItems();
                })
            }
        })
      }


    onchangecheckbox=()=>{
        var ref = document.getElementById("task")
        let checkdepartment =0
        var getTaskManagement = ref.getElementsByClassName('d-check')
        for(let i=0;i<getTaskManagement.length;i++){            
            if(getTaskManagement[i].checked === true){
                checkdepartment++               
              }             
        }
        if(checkdepartment >0){
            this.setState({add:true,delete:false}) 
        }else{
            this.setState({add:false,delete:true})
        }
    }



    search=(event)=>{
        let val = event.target.value;
 
        var queryResult=[];
        if(val !== ""){
        axios.get(config.port+"/users/searchtask",{params:{val:val}}).then(res=>{
      
            res.data.map((rows,index)=>{
            queryResult.push(rows);
            console.log("query result ",queryResult);
        })
        this.setState({task:queryResult})
        })
        }else{
            this.refreshItems();
        }
      }



render(){

    const getBadge = (status) => {
        return status === 'notstarted' ? 'primary' :
            status === 'complete' ? 'warning' :
                'primary'
            }

    const taskBadge =(status)=>{
                return status === 'notstarted' ? 'primary' :
                status === 'complete' ? 'light' :            
                        'primary'
    }        

    const Capitalize=(str)=>{
        return str.charAt(0).toUpperCase() + str.slice(1);
    }


    const paginationContent = (
        <div className="ml-5">
         <Pagination
            hideDisabled
            prevPageText='prev'
            nextPageText='next'
            firstPageText='first'
            lastPageText='last'
            activePage={this.state.activePage}
            itemsCountPerPage={this.state.pageSize}
            totalItemsCount={this.state.task.length}
            itemClass="page-item"
            linkClass="page-link"
            onChange={this.handlePageChange}
            />
          </div>
      )

      var pagination;
          if(this.state.task.length > this.state.pageSize){
            pagination = <div>{paginationContent}</div>
          }else{
            pagination = <div></div>
        }

    if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4><Link to='/task'><i className="fa fa-tasks"></i></Link>&nbsp;Task Management
                            <Link to='/taskmanage/add'><Button color="secondary" outline size="sm" type="submit" style={{float:"right"}} hidden={this.state.add}>
                            <i className="fa fa-plus" style={{color:"black"}}></i></Button></Link>
                            <a className="btn btn-danger btn-sm ml-2 float-right" hidden={this.state.delete} onClick={this.multipledelete}><i className="fa fa-trash" style={{color:"white"}} ></i></a>
                            </h4>
                        </CardHeader>
                        <CardBody>
                        
                            <div>
                            <InputGroup className="mb-2">
                                    <InputGroupAddon addonType="prepend"><Button><i className="fa fa-search"></i></Button></InputGroupAddon>
                                     <Input placeholder="Search" className="w-75" name="searchbar" onChange={this.search}/> 
                                    <Input type="select"  name="pageSize" id="pageSize" onChange={this.myChangeHandler} onMouseOut={this.myChangeHandler} >
                                        <option value="10">10</option>
                                        {this.state.task.length>10?<option value="15">15</option>:<span> </span>}
                                        {this.state.task.length>15?<option value="20">20</option>:<span> </span>}
                                        {this.state.task.length>20?<option value="25">25</option>:<span> </span>}
                                        {this.state.task.length>25?<option value="30">30</option>:<span> </span>}
                                     </Input>
                                </InputGroup>
                                
                            </div>
                            <form id="task">
                            <Table responsive hover>
                                <thead>
                                  <tr>
                                      
                                      <th scope="col">S.No</th>
                                      <th scope="col">Project Name</th>
                                      <th scope="col">Task Name</th> 
                                      <th scope="col">Employee Name</th>
                                      <th scope="col">Description</th>                                 
                                      <th scope="col">Status</th> 
                                      
                                         
                                      <th scope="col">Action</th>                             
                                    </tr>
                                </thead>   
                                <tbody>
                                    {this.state.items.map((task, index)=>{
                                        const editLink=`taskmanage/edit/${task.id}`
                                        const showlink=`taskmanage/show/${task.id}`
                                        var checked;
                                        task.status === "active" ? checked=true:checked=false;
                                        return(
                                            <tr key={index}>
                                                <td>
                                                {index+1}</td>
                                                <td>{task.project_name}</td>
                                                <td>{task.task_name}</td>
                                                <td>{task.emp_name}</td>
                                                <td>{task.discription}</td>
                                                                                     
                                                <td>
                                                {task.status === "in_progress" ?<img src={inProgress} className="my-2" alt="" width="50px" height="40px" title="In Progress"/>:""}
                                                {task.status === "not_started" ?<img src={notStarted} className="my-2" alt="" width="50px" height="40px" title="Not Started"/>:""}
                                                {task.status === "completed" ?<img src={completed} className="my-2" alt="" width="50px" height="40px" title="Completed"/>:""}
                                                </td>
                                                
                                                

                                                <td> 
                                                    <Link to={showlink}><Button className="btn-sm" color="primary"><i className="fa fa-eye" style={{color:"white"}}></i></Button>
                                                    </Link>&nbsp;&nbsp;
                                                    

                                                 <Link to={editLink}><Button className="btn-sm" color="primary"><i className="fa fa-pencil" style={{color:"white"}}></i></Button></Link>

                                                <a className="btn btn-danger btn-sm ml-2" onClick={()=>this.deleteTaskManagement(task.id)}><i className="fa fa-trash" style={{color:"white"}}></i></a>
                                                    
                                                

                                                </td>   
                                            </tr>
                                        )
                                    })}
                                </tbody>                            
                              </Table>   
                                <div className="ml-5">
                                    {pagination}
                                </div>                         
                          </form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default TaskManagement;