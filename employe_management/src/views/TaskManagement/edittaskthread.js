/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Table, Form, Button} from 'reactstrap';
import Moment from  'react-moment';
import { Redirect } from 'react-router-dom'
import config from '../../port.js';

class EditTaskThread extends Component{
    constructor(props){
        super(props)
        this.state = ({employee:[],task:[],emp_id:"0",task_management:[],task:"", task_id:"", date:"", hours:"", status:"",description:"",redirect:false})
     
    }

    componentDidMount(){


        axios.get(config.port+"/users/currentemp").then(res=>{
            //console.log(res.data)
            this.setState({employee:res.data})           
        })


        axios.get(config.port+"/users/showtaskmanagement").then(res=>{
            //console.log(res.data);
            this.setState({task_management:res.data})
        })

        axios.get(config.port+"/users/showtask").then(res=>{
            console.log(res.data)
            this.setState({taskThread:res.data})
         
             })
        
        let id = this.props.match.params.id;
        console.log(id)
        axios.get(config.port+"/users/showtaskbyid",{params:{id:id}}).then(res=>{
            console.log(id,res.data)
        if(res.data.length>0){
            let task = res.data[0]
            console.log(task);
           this.setState({emp_id:task.emp_id, task_id:task.task_id, date:task.date, hours:task.hours, status:task.status, description:task.description,redirect:false})
            }
        })
        console.log(this.state.task);
    }

    
    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
        console.log(this.state)
      }


    submit=(event)=>{ 
        event.preventDefault();
        console.log(this.state);
        let id = this.props.match.params.id;
        console.log("hello", id)
        axios.post(config.port+"/users/updateTaskThread",{

                    date:this.state.date,
                    hours:this.state.hours,
                    description:this.state.description,               
                    status:this.state.status,
                    id:id
          
        }).then(res=>{
            if(res.data.success === true){
                let path =`/taskmanage/show/${this.state.task_id}` 
                console.log("path",path)
                this.props.history.push(path)
            }
        })
    }

    renderRedirect = () => {
      

      }

    render(){

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Employee Id : {this.state.emp_id} <Moment format="MMMM DD, YYYY" className="float-right">{this.state.date}</Moment>
                               </h4>
                        </CardHeader>
                        <CardBody>
                            <Form onSubmit={this.submit}>
                            <Table borderless>                          
                              <tr>
                                  <th>date :</th>
                                  <td>
                                  <input type="date" name="date" className="w-50" value={this.state.date} onChange={this.myChangeHandler} />
                                  </td>
                              </tr>
                              <tr>
                                  <th>Description :</th>
                                  <td>
                                 <input name="description" value={this.state.description} onChange={this.myChangeHandler} className="w-50"/>
                                  </td>
                              </tr>
                              <tr>
                                   <th>Hours :</th>
                                    <td><input type="number" name="hours" value={this.state.hours} onChange={this.myChangeHandler} className="w-50"/></td>
                              </tr>
                              <tr>
                                   <th>Status :</th>
                                   <td><select  name="status" id="status" className="w-50" onChange={this.myChangeHandler} value={this.state.status} >
                                            <option value="0">Please Select Status</option>
                                            <option value="notstarted">Not Started </option>
                                            <option value="inprogress">In Progress</option>
                                            <option value="completed">Completed</option>
                                        </select>
                                    </td>
                              </tr>
                              <tr>
                                  <td colspan="2">
                                  {this.renderRedirect()}
                                  <Button type="submit" color="primary" className="float-right">Update</Button>
                                  </td>
                                  </tr>
                            </Table>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default EditTaskThread;