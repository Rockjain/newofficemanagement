
import Dashboard from './Dashboard';

import { Login, Page404, Page500, Register } from './Pages';
import { Widget03 } from './Widgets'
import Profile from './profile';
import { AddEmployee , ShowEmployee  } from './employee'

export {
  Widget03,
  Page404,
  Page500,
  Register,
  Login,
  Dashboard,
  Profile,
  AddEmployee,
  ShowEmployee 
};

