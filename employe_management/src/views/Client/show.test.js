import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import { mount } from 'enzyme'
import ShowClient from './edit';


it('renders without crashing', () => {
  const wrapper = mount(
    <MemoryRouter> 
      <ShowClient match={{params: {id: "1"}, isExact: true, path: "/client/show/:id", name: "Show Client"}}/>
    </MemoryRouter>
  );
  expect(wrapper.containsMatchingElement(<strong>Client</strong>)).toEqual(true)
  wrapper.unmount()
});