import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Client from './client';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><Client /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});