import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import AddTask from './addtask';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><AddTask /></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});