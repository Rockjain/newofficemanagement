/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Table, Form, Button} from 'reactstrap';
import Moment from  'react-moment';
import { Redirect } from 'react-router-dom'
import config from '../../port.js';

class EditTask extends Component{
    constructor(props){
        super(props)
        this.state = ({employee:[],emp_id:"0",task_management:[],task:"", task_id:"", date:"", hours:"", status:"",description:"", comments:"",redirect:false})
     
    }

    componentDidMount(){


        axios.get(config.port+"/users/currentemp").then(res=>{
            //console.log(res.data)
            this.setState({employee:res.data})           
        })


        axios.get(config.port+"/users/showtaskmanagement").then(res=>{
            //console.log(res.data);
            this.setState({task_management:res.data})
        })
        
        





        let id = this.props.match.params.id;
        console.log(id)
        axios.get(config.port+"/users/showtaskbyid",{params:{id:id}}).then(res=>{
            console.log(id)
        if(res.data.length>0){
            let task = res.data[0]
            console.log(task);
           this.setState({emp_id:task.emp_id, task_id:task.task_id, date:task.date, hours:task.hours, status:task.status, description:task.description,comments:task.comments,redirect:false})
            }
        })
        console.log(this.state);
    }

    
    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
        console.log(this.state)
      }


    submit=(event)=>{ 
        event.preventDefault();
        console.log(this.state);
        let id = this.props.match.params.id;
        console.log("hello", id)
        // axios.post(config.port+"/users/updateTasktable",{

        //             emp_id:this.state.emp_id,
        //             task_id:this.state.task_id,
        //             date:this.state.threaddate,
        //             hours:this.state.threadhours,
        //             description:this.state.threaddescription,               
        //             status:this.state.threadstatus,
        //             id:id
          
        // }).then(res=>{
        //     if(res.data.success === true){
        //         this.setState({redirect:true})
        //     }
        // })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/task' />
        }
      }

    render(){

if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Employee Id : {this.state.emp_id} <Moment format="MMMM DD, YYYY" className="float-right">{this.state.date}</Moment>
                               </h4>
                        </CardHeader>
                        <CardBody>
                            <Form onSubmit={this.submit}>
                            <Table borderless>                          
                              <tr>
                                  <th>date :</th>
                                  <td>
                                  <input type="date" name="threaddate" className="w-50" value={this.state.threaddate} onChange={this.myChangeHandler} />
                                  </td>
                              </tr>
                              <tr>
                                  <th>Description :</th>
                                  <td>
                                 <input name="threaddescription" value={this.state.threaddescription} onChange={this.myChangeHandler} className="w-50"/>
                                  </td>
                              </tr>
                              <tr>
                                   <th>Hours :</th>
                                    <td><input name="threadhours" value={this.state.threadhours} onChange={this.myChangeHandler} className="w-50"/></td>
                              </tr>
                              <tr>
                                   <th>Status :</th>
                                    <td>
                                        {this.state.threadstatus === "working"?<input type="radio"name="threadstatus" value={this.state.threadstatus} checked onChange={this.myChangeHandler}/>: <input type="radio"  name="threadstatus" value="working" onChange={this.myChangeHandler}/>} Working 
                                        {this.state.threadstatus === "completed"? <input type="radio" name="threadstatus" value={this.state.threadstatus} checked onChange={this.myChangeHandler}  className="ml-3" />: <input type="radio"  className="ml-3" name="threadstatus" value="completed" onChange={this.myChangeHandler}/>} Completed </td>
                              </tr>
                              <tr>
                                  <td colspan="2">
                                  {this.renderRedirect()}
                                  <Button type="submit" color="primary" className="float-right">Submit</Button>
                                  </td>
                                  </tr>
                            </Table>
                            </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default EditTask;