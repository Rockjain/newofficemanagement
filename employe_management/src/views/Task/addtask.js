/* eslint-disable array-callback-return */
import React, { Component } from 'react';
import axios from 'axios';
import {  Card, CardBody, CardHeader, Col, Row, Button, Table, Form} from 'reactstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Redirect } from 'react-router-dom';
import config from '../../port.js';

class AddTask extends Component{
    constructor(props){
        super(props)
        this.state = ({employee:[],task_management:[],name:"",task_name:"",project_id:"",emp_id:"",task_id:"",startDate:new Date(),hours:"",description:"",status:"",comments:"",submit:true,update:true,messgae:"",redirect:false})
        
    }

    componentDidMount(){       
        axios.get(config.port+"/users/currentemp").then(res=>{
            console.log(res.data)
            this.setState({employee:res.data})           
        })


        axios.get(config.port+"/users/showtaskmanagement").then(res=>{
            console.log(res.data);
            this.setState({task_management:res.data})
        })       
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
       // console.log(this.state)

    }


    handleChange=(date)=>{
        this.setState({
          startDate: date
        });
      }
      

     

     
         

      handletimeChange=idx=>evt=>{
        const time = this.state.time.map((time, sidx) => {
            if (idx === sidx){
                return evt.target.value ;
                }else{
                    return time 
                }            
          });
         
          this.setState({time:time});
          console.log(this.state.time)
        }


      addtask=(event)=>{       
        event.preventDefault();
            if(this.state.emp_id === ""){
                alert("Please Select Employee Name")
            }else{
                axios.post(config.port+"/users/addtask",{
                   
                    emp_id:this.state.emp_id,
                    task_id:this.state.task_id,
                    date:this.state.startDate,
                    hours:this.state.hours,
                    description:this.state.description,               
                    status:this.state.status,
                    comments:this.state.comments
                }).then(res=>{
                    console.log(res.data);
                   if(res.data.success===true){
                       this.setState({redirect:true})
                   }
                })
        }
    }
           
    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/task' />
        }
      }

render(){
      if(localStorage.getItem("token")){
        return (
            <div className="animated fadeIn">
                
                <Row>
                    <Col>
                        <Card>
                        <CardHeader>
                            <h4>Assigne Task
                                  <span className="float-right"><DatePicker
                                    selected={this.state.startDate}
                                    onChange={this.handleChange}
                                    onSelect={this.handleSelect}
                                    dateFormat="MMMM dd, yyyy"
                                    className=" pl-1 float-right"/> </span>
                            </h4>
                        </CardHeader>
                        <CardBody>
                        <Form onSubmit={this.addtask}>
                            <Table borderless>
                            <tbody>
                                         
                                
                                <tr>
                                    <th>Employee Name :</th>
                                     <td>
                                        <select name="emp_id" id="emp_id" onChange={this.myChangeHandler} className="w-50" value={this.state.emp_id}>
                                        <option value="0">Please Select Employee Name</option>
                                        {
                                        this.state.employee.map((employee,key)=>{
                                         return <option value={employee.id}>{employee.name}</option>
                                        })
                                        }
                                                   
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Task Name :</th>
                                     <td>
                                        <select name="task_id" id="task_id" onChange={this.myChangeHandler} className="w-50" value={this.state.task_id}>
                                        <option value="0">Please Select Task Name</option>    
                                        {
                                        this.state.task_management.map((task_management,key)=>{
                                         return <option value={task_management.id}>{task_management.task_name}</option>
                                        })
                                        }          
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Date :</th>
                                    <td>
                                    <input type="date" name="date" className="w-50" value={this.state.date} onChange={this.myChangeHandler} />

                                    </td>
                                </tr>
                                <tr>
                                    <th>Hours :</th>
                                    <td>
                                    <input type="text" id="hours" name="hours" className="w-50" value={this.state.hours} onChange={this.myChangeHandler}
                                     min="09:00" max="18:00" />

                                    </td>
                                </tr>
                               <tr>
                                    <th>Description :</th>
                                     <td><input type="textarea" name="description" className="w-50" value={this.state.description} placeholder="Description About Task"  onChange={this.myChangeHandler}/></td>
                                </tr>
                                                                                                                                                 <tr>
                                        <th>Status :</th>
                                        <td><input type="radio" name="status" value="not_started" onChange={this.myChangeHandler}/> Not Started 
                                        <input type="radio" name="status" className="ml-3" value="in_progress" onChange={this.myChangeHandler}/> In Progress
                                        <input type="radio" name="status" className="ml-3" value="completed" onChange={this.myChangeHandler}/> Completed
                                        </td>
                                    </tr>
                                         <tr>
                                             <th>Comments :</th>
                                             <td><input type="text" name="comments" className="w-50" value={this.state.comments} placeholder="Any Comments About Task" onChange={this.myChangeHandler}/></td>
                                         </tr>
     
                                     </tbody>
                            </Table>
                                    {this.renderRedirect()}
                                     <Button color="primary" className="float-right">Submit</Button>
                                </Form>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div>
        )
    }else{
        return(
            <div>
               <Row>
                        <Col>
                            <Card>
                              <CardHeader>
                                  <h4>Message</h4>
                              </CardHeader>
                              <CardBody>
                                  Sorry you are not loggedin.
                                
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
              </div>
          )
    }
}
}

export default AddTask;